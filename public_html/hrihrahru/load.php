<?php

// ---------------------------------- //
// Load of forms from cbr.ru to MySQL //
// ---------------------------------- //

ini_set('max_execution_time', 600);
ini_set('error_reporting', E_ALL);
ini_set('display_errors', 'On');



// Master data
// -----------

$forms = array(101, '101+', 102, '102+', 123, 135);
//$forms = array(101, '101+', 123, 135);
//$forms = array(135);


// Range of dates
// --------------
$start_date = strtotime('2017-06-01');
$end_date   = strtotime('2017-07-01');

$start_year = date('Y', $start_date);
$end_year = date('Y', $end_date);

$report_dates = array();

foreach(range($start_year, $end_year) as $year) {
  foreach(range(1, 12) as $month) {
    $date = mktime(0, 0, 0, $month, 1, $year);
    
    if($start_date <= $date && $date <= $end_date) {
      $report_dates[] = date('Y-m-01', $date);
    }
  }
}

$quarters = array('01', '04', '07', '10');

// my_print_r($report_dates);



// Useful functions
// ----------------
function my_print_r($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}



// Connect to MySQL
// ----------------
$db = new PDO('mysql:host=localhost;dbname=bankihrj_db', 'bankihrj_loader', 'Tycmkdpvslp6809463#');
$db->query("SET NAMES UTF8");



// Iterate through reporting dates
// -------------------------------
foreach($report_dates as $report_date) {

  // Dates
  // -----
  $rar_date = date("Ym", strtotime($report_date));

  $file_date = strtotime($report_date) - 1;
  $pl_file_date = ceil(date("m", $file_date)/3) . date("Y", $file_date);
  $bs_file_date = date("mY", $file_date);



  // File names
  // ----------
  $dbase[101] = "{$bs_file_date}B1.DBF";
  $dbase['101+'] = "{$bs_file_date}_B.DBF";
  $dbase[102] = "{$pl_file_date}_P1.DBF";
  $dbase['102+'] = "{$pl_file_date}_P.DBF";
  $dbase[123] = "{$bs_file_date}_123D.DBF";
  $dbase[134] = "{$bs_file_date}_134D.dbf";
  $dbase[135] = "{$bs_file_date}_135_3.dbf";



  // Indexes of metrics in DBFs
  // --------------------------
  $acc = array(
    101			=> 2,
    '101+'	=> 2,
    102			=> 1,
    '102+'	=> 1,
    123			=> 1,
    134			=> 1,
    135			=> 1
  );

  $tot = array(
    101			=> 15,
    '101+'	=> 3,
    102			=> 4,
    '102+'	=> 2,
    123			=> 2,
    134			=> 2,
    135			=> 2
  );



  // Load data
  // ---------
  foreach($forms as $form) {
    
    // If not this situation: form 102 with non-quarter date
    if(!(($form === 102 || $form === '102+') && !in_array(substr($report_date, 5, 2), $quarters))) {  
  
      // 3-digit name of form
      $frm = intval($form);
  

      // Скачиваем и открываем архив
      if($form !== '101+' && $form !== '102+') {
        file_put_contents("$frm-{$rar_date}01.rar", fopen("http://cbr.ru/credit/forms/$frm-{$rar_date}01.rar", 'r'));
        $rar_file = rar_open("$frm-{$rar_date}01.rar");
      }
  
  
      // Распаковываем нужные файлы
      $entry = rar_entry_get($rar_file, $dbase[$form]);
      $entry->extract(".");
  
  
      // Закрываем архив
      if($form === '101+' || $form === '102+') {
        rar_close($rar_file);
      }
  
  
      // Загружаем данные
      $dbf = dbase_open($dbase[$form], 0);
      $num_records = dbase_numrecords($dbf);
      
      $data = array();
      $j = 0;						// счетчик записей для заполнения порций данных для отправки в MySQL
      
      for($i = 1; $i <= $num_records; ++$i) {
    
        ++$j;
    
        $record = dbase_get_record($dbf, $i);
    
        // Читаем данные
        $regnum = $record[0];
        $account = $record[$acc[$form]];
        
        $dr_total = 0;
        
        $cr_total = 0;
        
        $total    = $record[$tot[$form]];
  
        if($form === 101) {
    
          $date = substr_replace($record[16], "-", 4, 0);
          $date = substr_replace($date, "-", -2, 0);
          
          if($account == 405) {
            $account = 40500;
          }
          
          if ($account == 406) {
            $account = 40600;
          }
          
          if ($account == 407) {
            $account = 40700;
          }
          
          if ($account == '408.1') {
            $account = 40800;
          }
          
          if ($account == '408.2') {
            $account = 40899;
          }            

          $dr_total = $record[9];
          
          $cr_total = $record[12];
    
          
          // Меняем знак оборотов
          
          $cr_total = -$cr_total;
          
          
          // Меняем знак пассивов
          
          $bs_part = $record[3];
          
          if($bs_part == 2) {
            $total = -$total;
          }
    
        } elseif($form === '101+') {
    
          $date = substr_replace($record[5], "-", 4, 0);
          $date = substr_replace($date, "-", -2, 0);
          
          // Делаем значения пассивов отрицательными
          $bs_part = $record[4];
          if($bs_part == 2) {
            $total = -$total;
          }
    
        } elseif($form === 102 || $form === '102+') {
    
          $date = $report_date;
          
          // Делаем значения расходов отрицательными
          if($account >= 30000 && $account <= 49999) {
            $total = -$total;
          }
      
          $negatives = array(51101, 51201);
          if(in_array($account, $negatives))
          {
            $total = -$total;
          }
      
          // Обнуляем счета ххх00 (подитоги)
          if(preg_match('/00$/', $account)) {
            $total = 0;
          }
            
        } elseif($form === 123) {
    
          $account = preg_replace("/\x20/", "", $account); // исправляем UTF-8 удаляя из него пробелы
          $account = iconv("cp866", "UTF-8", $account);
          $date = $report_date;
          
          // Change sign of accounts 201.8, 202.5, 101.14 and 104.8
          if(preg_match('/^101\.|^104\.|^201\.|^202\./', $account)) {
            $total = -$total;
          }
    
        } elseif($form === 134) {
    
          $account = preg_replace("/\x20/", "", $account); // исправляем UTF-8 удаляя из него пробелы
          $date = $report_date;
          
          // Change sign of accounts 109-115, 208, 30* and 50*
          if(preg_match('/^109|^110|^111|^112|^113|^114|^115|^208|^30|^50/', $account)) {
            $total = -$total;
          }
          
          // Remove "U" from the beginning of some accounts
          if(substr($account, 0, 1) === "U") {
              $account = substr($account, 1);
          }
    
        } elseif($form === 135) {
          $account = preg_replace("/[\x20\x8d]/", "", $account); // исправляем UTF-8 удаляя из него пробелы и еще что-то (8d)
          $date = $report_date;
          
          /*if(array_key_exists($account, $_135)) {
            $account = $_135[$account];
          } else {
            $total = 0;
          }*/
        }
    
    
        // Берем только ненулевые значения
        if($dr_total == 0 && $cr_total == 0 && $total == 0) {
          // do nothing
        } else {
        	//if($regnum == 1481) {
	          $data[] = "('$regnum', '$date', '$frm', '$account', $dr_total, $cr_total, $total)";
	        //}
        }
        
        // Отправляем порцию в MySQL
        if($j == 10000 || $i == $num_records) {
          $query = "INSERT INTO data VALUES " . implode(",", $data);
          $db->query($query);
      
          $j = 0;
          $data = array();
          $query = "";
        }

      } // end for
  
      echo "$form done<br>\n";
    
    } // end of if
    
  } // end foreach $forms
  
  echo "$report_date done<br><br>\n";
  
} // end foreach $report_dates
