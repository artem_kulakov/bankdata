<?php

// ----------------------- //
// Processing data for P&L //
// ----------------------- //



// Provisions on overdue loans
// ---------------------------
/*$overdue_loans = array();

foreach($dates as $date) {
	// Calculate overdue corporate loans
	foreach($instruction['Активы']['Кредиты и авансы клиентам']['Кредиты юридическим лицам']['Просроченные кредиты'] as $account) {
		$overdue_loans[$date]['corporate'] += $data[$date][$account];
	}
	
	// Calculate overdue retail loans
	foreach($instruction['Активы']['Кредиты и авансы клиентам']['Кредиты физическим лицам']['Просроченные кредиты'] as $account) {
		$overdue_loans[$date]['retail'] += $data[$date][$account];
	}
	
	// Divide provisions between corporate and retail, and attach to MySQL data
	$data[$date]['45818/Ю'] = $data[$date][45818] / ($overdue_loans[$date]['corporate'] + $overdue_loans[$date]['retail']) * $overdue_loans[$date]['corporate'];
	$data[$date]['45818/Ф'] = $data[$date][45818] / ($overdue_loans[$date]['corporate'] + $overdue_loans[$date]['retail']) * $overdue_loans[$date]['retail'];
}

// my_print_r($overdue_loans);*/



// List of IDs
// -----------
$before_lists = microtime(true);
$before_ids = microtime(true);

function get_id($instruction, $parent_id) {
	global $id_list;
	$counter = 0;
	foreach($instruction as $item) {
		$id = $parent_id . '-' . $counter;
		$id_list[] = $id;
		++$counter;
		if(is_array($item)) {
			get_id($item, $id);
		}
	}
}

// Calling get_id function
$id_list = array();
get_id($instruction, '');

// Removing leading "-" from ID
foreach($id_list as $key => $item) {
	$id_list[$key] = substr($item, 1);
}

$after_ids = microtime(true);

// my_print_r($id_list);



// List of values
// --------------
$before_values = microtime(true);

// Calling get_values function for each date
foreach($data as $date => $one_date_data) {
	$values_list[$date] = array();
	
	// Key for the total of whole BS, which will be removed from $values_list array
  $key = -1;
  
	get_values($instruction, $key);
	
	// Remove the total of whole BS and sort $values_list array by the key
  unset($values_list[$date][-1]);
  ksort($values_list[$date]);	
}

// Function get_values
function get_values($array, $array_key) {
  
  global $values_list, $key, $date, $one_date_data;
  
  $total = 0;

  foreach($array as $index => $item) {
    
    if(is_array($item)) {
      ++$key;
      $total += get_values($item, $key);
    } else {
      ++$key;
      $value = $one_date_data[$item];
      $total += $value;
      $values_list[$date][$key] = $value;
    }
  }
  
  // Change negative value to zero for this lines (via the last element of the line):
  // 102 (via 101.14)
  // 105 (via 104.8)
  // 203 (via 202.5)
  if(($index ==='101.14' || $index ==='104.8' || $index ==='202.5') && $total < 0) {
    $total = 0;
  }
  
  $values_list[$date][$array_key] = $total;
  
  return $total;
}

// my_print_r($values_list);

$after_values = microtime(true);



// List of names and accounts, indexes of lines with accounts
// ----------------------------------------------------------
function get_names($instruction) {
	global $names_list, $accounts_list, $accounts_names, $indexes_of_accounts;
	foreach($instruction as $key => $item) {
		if(is_array($item)) {
			$names_list[] = $accounts_names[$key];
			$accounts_list[] = $key;
			get_names($item);
		} else {
			$names_list[] = $accounts_names[$item];
			$accounts_list[] = $item;
			
			// Get indexes of lines with accounts
			$indexes_of_accounts[] = count($names_list)-1;
		}
	}
}

// Calling get_names function
$names_list = array();
$accounts_list = array();
$indexes_of_accounts = array();
get_names($instruction);

// my_print_r($names_list);
// my_print_r($accounts_list);
// my_print_r($indexes_of_accounts);



// List of classes
// ---------------
$classes_list = array();
foreach($id_list as $key => $item) {
	$number = substr_count($item, '-');
	if($number > 0) {
		if(in_array($key, $indexes_of_accounts)) {
			$classes_list[] = " class='account x" . $number . "0'";
		} else {
			$classes_list[] = " class='x" . $number . "0'";
		}
	} else {
		$classes_list[] = "";
	}
}

// my_print_r($classes_list);



/*// Remove empty and zero lines
// ---------------------------
foreach($id_list as $index => $id) {
	$line_is_empty = true;
	
	foreach($dates as $date) {
		$this_value = $values_list[$date][$index];
		if($this_value != 0 && $this_value != '') {
			$line_is_empty = false;
		}
	}
	
	if($line_is_empty == true) {
		unset($id_list[$index]);
	}
}*/

$after_lists = microtime(true);
