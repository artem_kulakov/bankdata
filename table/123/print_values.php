<?php

// ------------------ //
// Print table values //
// ------------------ //



function print_values() {
  global $id, $changes, $columns, $values_list, $multiple, $index, $indexes_of_totals;
  
  // Value
  foreach($columns as $date) {
    
    // Output parameters for change
    if($date == $changes[0]) {
      $decimals = 0;
      $mult = $multiple;
      $unit = "";
      
      // Positive sign for positive change
      if($values_list[$date][$index] > 0) {
        $plus = "+";
      } else {
        $plus = "";
      }
      
    // Output parameters for % change
    } elseif($date == $changes[1]) {
      $decimals = 1;
      $mult = 1;
      $unit = "%";
      
      // Positive sign for positive change
      if($values_list[$date][$index] > 0) {
        $plus = "+";
      } else {
        $plus = "";
      }
      
    } else {
      $decimals = 0;
      $mult = $multiple;
      $unit = "";
    }
    
    // Format value
    $value = $plus . number_format($values_list[$date][$index] / $mult, $decimals, ',', ' ') . $unit;
    
    // Show zero values as '-'
    if($value == '0' || $value == '0,0%') {
      $value = "-";
    }
    
    echo "<td>{$value}</td>";
  }
  
  // Closing tr
  echo "</tr>";
}
