<?php

// Equity instruction
// ------------------

$instruction = array(
  '116' => array(
    '108' => array(101, 102, 103, 104, 105, 106, 107),
    109,
    110,
    111,
    112,
    113,
    114,
    115
  ),
  '210' => array(
    '209' => array(201, 202, 203, 204, 205, 206, 207, 208)
  ),
  '300' => array(301, 302),
  '400' => array(),
  501,
  502,
  503,
  '000' => array()
);



// Useful functions
// include_once 'functions.php';



// Simplify for export
// -------------------

$full_instruction = $instruction;

if($export) {
  foreach($instruction as $name_1 => $items) {
    foreach($items as $name_2 => $content) {
      if(is_array($content)) {
      
        array_walk_recursive($content, 'support');
      
        $instruction[$name_1][$name_2] = $support;
      
        $support = array();
      }
    }
  }
}

function support($value, $key) {
  global $support;
  $support[] = $value;
}



// Totals
// ------
$lines = array();
$totals = array();

foreach($instruction as $name => $line) {
  
  if(!empty($line)) {
    
    if(is_array($line)) {
      $lines[] = $name;
    } else {
      $lines[] = $line;
    }
    
  } else {
    $totals[$name] = $lines;
  }
}

// my_print_r($totals);



// Test instruction
// my_print_r($instruction);
