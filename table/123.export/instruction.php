<?php

// Equity instruction
// ------------------

$instruction = array(
  '102' => array(
    '100.7' => array(
      '100.1',
      '100.2',
      '100.3',
      '100.4',
      '100.5',
      '100.6'
    ),
    '101.14' => array(
      '101.1',
      '101.2',
      '101.3',
      '101.4',
      '101.5',
      '101.6',
      '101.7',
      '101.8',
      '101.9',
      '101.10',
      '101.11',
      '101.12',
      '101.13'
    )
  ),
  '105' => array(
    '103.5' => array(
      '103.1',
      '103.2',
      '103.3',
      '103.4'          
    ),
    '104.8' => array(
      '104.1',
      '104.2',
      '104.3',
      '104.4',
      '104.5',
      '104.6',
      '104.7'
    )
  ),
  '106' => array(),
  '200.9' => array(
    '200.1',
    '200.2',
    '200.3',
    '200.4',
    '200.5',
    '200.6',
    '200.7',
    '200.8'
  ),
  '201.8' => array(
    '201.1',
    '201.2',
    '201.3',
    '201.4',
    '201.5',
    '201.7'
  ),
  '202.5' => array(
    '202.1',
    '202.2',
    '202.3',
    '202.4'
  ),
  '203' => array(),
  '000' => array()
);



// Useful functions
// include_once 'functions.php';



// Simplify for export
// -------------------

$full_instruction = $instruction;

if($export) {
  foreach($instruction as $name_1 => $items) {
    foreach($items as $name_2 => $content) {
      if(is_array($content)) {
      
        array_walk_recursive($content, 'support');
      
        $instruction[$name_1][$name_2] = $support;
      
        $support = array();
      }
    }
  }
}

function support($value, $key) {
  global $support;
  $support[] = $value;
}



// Totals
// ------
$lines = array();
$totals = array();

foreach($instruction as $name => $line) {
  if(!empty($line)) {
    $lines[] = $name;
  } else {
    $totals[$name] = $lines;
    $lines = array();
  }
}

// my_print_r($totals);



// Test instruction
// my_print_r($instruction);
