<?php


// List of IDs
// -----------

$counter_1 = 0;

foreach($instruction as $item_1) {
  
  $id_list[] = $counter_1;
  
  $counter_2 = 0;
  
  foreach($item_1 as $item_2) {
    
    if(is_array($item_2)) {
      
      $id_list[] = $counter_1 . '-' . $counter_2;
    
      ++$counter_2;
    }
  }
  
  ++$counter_1;
}

// my_print_r($id_list);



// List of values
// --------------

foreach($data as $date => $one_date_data) {

  foreach($instruction as $name_1 => $item_1) {
    
    $values_list[$date][] = 0;
    $key = count($values_list[$date]) - 1;
    
    $total_1 = 0;
    
    foreach($item_1 as $name_2 => $item_2) {
      
      if(is_array($item_2)) {
        
        $total_2 = 0;
        
        foreach($item_2 as $item_3) {
        
          $total_2 += $one_date_data[$item_3];
        }
      	
        $values_list[$date][] = $total_2;
      
        $total_1 += $total_2;
        
      } else {
        
        $total_1 += $one_date_data[$item_2];
      }
    }
    
		if(preg_match("/^102$|^105$|^203$/", $name_1) && $total_1 < 0) {
			$total_1 = 0;
		}
    
    $values_list[$date][$key] = $total_1;
  }
}

// my_print_r($values_list);



// List of names
// -------------

foreach($instruction as $name_1 => $item_1) {
  
  $names_list[] = $accounts_names[$name_1];
  $names_list_2[] = "";
  $accounts_list[] = $name_1;
  
  foreach($item_1 as $name_2 => $item_2) {
    
    if(is_array($item_2)) {
      $names_list[] = "";
      $names_list_2[] = $accounts_names[$name_2];
      $accounts_list[] = $name_2;
    }
  }  
}

// my_print_r($accounts_list);
