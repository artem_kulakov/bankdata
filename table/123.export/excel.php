<?php


// Include PHPExcel
require_once $_SERVER['DOCUMENT_ROOT'] . '/Classes/PHPExcel.php';



// Create new PHPExcel object
$objPHPExcel = new PHPExcel();



// Set document properties
$objPHPExcel->getProperties()->setCreator("bankrep.ru")->setLastModifiedBy("bankrep.ru");



// Write data to worksheet
$objPHPExcel->setActiveSheetIndex(0);
$objPHPExcel->getActiveSheet()->fromArray($exported, null, 'A1');



// Format
// ------

// Columns width

$objPHPExcel->getActiveSheet()->getColumnDimension('A')->setWidth(30);
$objPHPExcel->getActiveSheet()->getColumnDimension('B')->setWidth(30);

$column_after_last = chr(ord('C') + count($columns));

for($column = 'C'; $column !== $column_after_last; $column++) {
  $objPHPExcel->getActiveSheet()->getColumnDimension($column)->setWidth(15);
}


// Bank name

$styleArray = array(
  'font'  => array(
    'bold'  => true,
    'size' => '14'
  )
);

$objPHPExcel->getActiveSheet()->getStyle('A1')->applyFromArray($styleArray);



// Header

$styleArray = array(
  'borders' => array(
    'bottom' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
  'font'  => array(
    'bold'  => true
  ),
  'alignment' => array(
		'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
    'wrap' => true
	)
);

$last_column = chr(ord('C') + count($columns) - 1);

$objPHPExcel->getActiveSheet()->getStyle("A5:{$last_column}5")->applyFromArray($styleArray);

$objPHPExcel->getActiveSheet()->getStyle("A5:B5")->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);



// Totals

$styleArray = array(
  'borders' => array(
    'bottom' => array(
      'style' => PHPExcel_Style_Border::BORDER_THIN
    )
  ),
  'font'  => array(
    'bold'  => true
  )
);

foreach($indexes_of_totals as $index) {

  $row = $index + 6;

  $objPHPExcel->getActiveSheet()->getStyle("A{$row}:{$last_column}{$row}")->applyFromArray($styleArray);
}



// Autofilter

// $objPHPExcel->getActiveSheet()->setAutoFilter('A1:A1275');



// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('Капитал (123)');



// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);



// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename="equity_123.xlsx"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
