<?php

// Reporting errors
// ----------------
ini_set('error_reporting', 0);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'Off');
//ini_set('error_log', '/tmp/php-error.log');



$start = microtime(true);

setlocale(LC_ALL,'ru_RU');


// Connect to MySQL
// ----------------
$db = new PDO('mysql:host=localhost;dbname=bankihrj_db', 'bankihrj_user', 'Ybksibkkvmnnp6809463#');
$db->query("SET NAMES UTF8");



// Useful functions
// ----------------

function my_print_r($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}


function sanitize($var) {
  global $db;
  
	$var = strip_tags($var);
	$var = htmlentities($var, ENT_QUOTES, "UTF-8");
	$var = stripslashes($var);
	return substr($db->quote($var), 1, -1);
}



// Sanitize query string
// ---------------------

foreach($_GET as $name => $item) {
  
  if(is_array($item)) {
    
    foreach($item as $subname => $subitem) {
      $clean[$name][$subname] = sanitize($subitem);
    }
  
  } else {
    $clean[$name] = sanitize($item);
  }
}

// my_print_r($clean);



// Define bank
// -----------

$top_banks = array(1481, 1000, 354, 1623, 2748, 3349, 2209, 1, 3466, 3251, 3292, 2272, 1978, 2289);

if(isset($clean['q'])) {
  
  // User input bank id
  if(is_numeric($clean['q'])) {
    $bank_id = $text = $clean['q'];
    $query = "SELECT id, name FROM banks_new WHERE id='{$bank_id}'";
  
  // User input bank name
  } else {
    
    // User submitted default text
    if($clean['q'] == "Название или рег. номер банка") {
      
      $bank_id = $clean['i'];
      $text = "Название или рег. номер банка";
      $query = "SELECT id, name FROM banks_new WHERE id='{$bank_id}'";
      
    // User submitted blank textbox
    } elseif($clean['q'] == "") {
      
      $bank_id = $clean['i'];
      $text = "";
      $query = "SELECT id, name FROM banks_new WHERE id='{$bank_id}'";
      
    // User input real words
    } else {
      
      $bank_name = $text = $clean['q'];
      
      $bank_name_query = preg_split('/\s+/', $bank_name);
      
      foreach($bank_name_query as $part) {
        $parts_name[]  = "name LIKE '%{$part}%'";
        $parts_short[] = "short LIKE '%{$part}%'";
      }
      
      $query_name  = implode($parts_name, " AND ");
      $query_short = implode($parts_short, " AND ");
      
      $query = "SELECT id, name FROM banks_new WHERE ({$query_name}) OR ({$query_short})";
    }
  }
  
} else {
  $bank_id = $top_banks[mt_rand(0, count($top_banks)-1)];
  
  $text = "Название или рег. номер банка";
  $query = "SELECT id, name FROM banks_new WHERE id='{$bank_id}'";
}



// Define dates
// ------------

$dates = array();
$http_dates_query = "";

if(isset($clean['d'])) {
  $chosen_dates = $clean['d'];
  
  foreach($chosen_dates as $d) {
    
    $year  = substr($d, 0, 2);
    $month = substr($d, 2, 2);
    
    // CBR date
    if($month == 12) {
      $month = 1;
      ++$year;
    } else {
      ++$month;
    }
    
    $dates[] = "20" . $year . "-" . sprintf("%02d", $month) . "-01";
    
    $http_dates_query .= "&d%5B%5D={$d}";
  }
} else {
  $dates = array('2017-06-01', '2017-07-01');
}



// Turnover output
// ---------------

// Start page

if(!isset($clean['m'])) {
  $start_page = true;
} else {
  $start_page = false;
}


// 2+ dates

if(count($dates) > 1) {
  $few_dates = true;
} else {
  $few_dates = false;
}


// Turnover available

$no_turnover = array(1326, 2726, 2249);

if(($start_page || $clean['m'] == 101) && !in_array($bank_id, $no_turnover)) {
  $turnover_available = true;
} else {
  $turnover_available = false;
}


// Turnover requested

if(!isset($clean['t'])) {
  $turnover_requested = false;
} else {
  $turnover_requested = true;
}


// echo "Start page: {$start_page}<br>";
// echo "Turnover available: {$turnover_available}<br>";
// echo "Turnover requested: {$turnover_requested}<br>";



// Adjust list of dates to download
// --------------------------------

$dates_to_download = $dates;

$quarters = array(1, 4, 7, 10);


// For 101: add dates between last two dates chosen by the user

if($turnover_available && $turnover_requested && $few_dates) {
  
  $last_date = end($dates);
  $second_last_date = prev($dates);
  
  $year  = substr($last_date, 0, 4);
  $month = substr($last_date, 5, 2);
  
  // Dates of turnovers to be summed up
  $dates_turnover[] = $last_date;
  
  
  do {
  
    --$month;
  
    if($month == 0) {
      $year -= 1;
      $month = 12;
    }
  
    $date = "{$year}-{$month}-01";
    
    if($date == $second_last_date) {
      
      // do nothing
      
    } else {
    
      $dates_to_download[] = $date;
      $dates_turnover[] = $date;
    }
  } while (strtotime($date) > strtotime($second_last_date));


// For 102: remove non-quarter dates

} elseif(isset($clean['m']) && ($clean['m'] == 102 || $clean['m'] == 216)) {
  
  $dates_to_download = array();
  
  foreach($dates as $date) {
    
    $month = substr($date, 5, 2);
    
    if(in_array($month, $quarters)) {
      if($clean['m'] == 102 && (strtotime($date) <= strtotime('2016-01-01')) || $clean['m'] == 216 && (strtotime($date) > strtotime('2016-01-01'))) {
        $dates_to_download[] = $date;
      }
    }
  }
  
  if(empty($dates_to_download)) {
    if($clean['m'] == 102) {
      $dates_to_download = array('2016-01-01');
    } elseif($clean['m'] == 216) {
      $dates_to_download = array('2017-07-01');
    }
  }
  

// For Ratios: add beginning of the period

} elseif(isset($clean['m']) && $clean['m'] == 'ratios') {
  foreach($dates as $date) {
    
    $year  = substr($date, 0, 4);
    $month = substr($date, 5, 2);
    
    if($month == "01") {
      $year -= 1;
    }
    
    $dates_to_download[] = "{$year}-01-01";
  }
}

$dates_to_download = array_unique($dates_to_download);

// my_print_r($dates_turnover);



// Define section of financials
// ----------------------------

if(isset($clean['m'])) {
  $mode = $clean['m'];
} else {
  $mode = $form = 101;
}

$form = "form='{$mode}'";


// Exception for 216 and 'ratios'
if($mode == 216) {
  $form = "form='102'";
} elseif($mode == "ratios") {
  $form = "(form='101' OR form='102')";
}



// If turnover requested
// ---------------------

if($turnover_requested) {
  $turnover = " checked";
  $turnover_query = "&t=1";
} else {
  $turnover = "";
  $turnover_query = "";
}



// Define scale
// ------------

if(isset($clean['s']) && in_array($clean['s'], array(1, 1000, 1000000))) {
  $multiple = $clean['s'];
} else {
  $multiple = 1000;
}

$scale = array(
  1       => 'В тысячах рублей',
  1000    => 'В миллионах рублей',
  1000000 => 'В миллиардах рублей'  
);



// Export to Excel?
// ----------------

if(isset($clean['x']) && $clean['x'] == 1) {
  $export = true;
} else {
  $export = false;
}



// Get bank name from MySQL
// ------------------------
$st = $db->query($query);
$results = $st->fetchAll();

// No bank found
if(count($results) == 0) {
  $bank_name = "";
  $message = "Не найден банк с таким регномером";

// One bank found
} elseif(count($results) == 1) {
  $bank_name = $results[0]['name'];
  $bank_id = $results[0]['id'];
  $message = "";

// Several banks found
} else {
  
  $bank_name = "";
  
  $banks_results = $results;
  
  $several_banks = true;
}



// Prepare dates for MySQL query
// -----------------------------
$dates_for_query = array();

foreach($dates_to_download as $date) {
  $dates_for_query[] = "date='{$date}'";
}

$dates_query = implode(' OR ', $dates_for_query);



// Get data from MySQL
// -------------------
$st = $db->query("SELECT date, form, account, dr_total, cr_total, total FROM data WHERE id='{$bank_id}' AND ({$dates_query}) AND {$form}");
$results = $st->fetchAll();

foreach ($results as $result) {
  if($mode == 'ratios') {
    
    $data[$result['form']][$result['date']][$result['account']] = $result['total'];
    
  } elseif($mode == '101') {
    
  	$data[$result['date']][$result['account']] = $result['total'];
  	
  	if(in_array($result['date'], $dates_turnover)) {
  	  
  	  if(isset($data['dr'][$result['account']])) {
      	$data['dr'][$result['account']] += $result['dr_total'];
      } else {
        $data['dr'][$result['account']] = $result['dr_total'];
      }
      
  	  if(isset($data['cr'][$result['account']])) {
      	$data['cr'][$result['account']] += $result['cr_total'];
      } else {
        $data['cr'][$result['account']] = $result['cr_total'];
      }
  	}
  } else {
    
  	$data[$result['date']][$result['account']] = $result['total'];
  }
}

// my_print_r($data);



// Starting HTML
// -------------

$sections = array(
  '101'    => "Баланс",
  '102'    => "Фин. результаты",
  '216' => "Фин. результаты (2016)",
  '123'    => "Капитал (123)",
  '134'    => "Капитал (134)",
  '135'    => "Нормативы",
  'ratios' => "Расчетные показатели"
);

if(!$export) {
  echo "<!doctype html><html><head>";
  echo "<meta charset='utf-8'>";
  echo "<meta name='description' content='Банковская отчетность в наиболее полном и удобном формате.'/>";
}



// Title
// -----

if(isset($clean['q'])) {
  $title = "{$bank_name} - {$sections[$mode]}";
} else {  
  $title = "Банкреп.ру: Отчетность банков в удобном формате";
}

if(!$export) {
  
  echo "<title>{$title}</title>";

  echo "<link rel='stylesheet' href='/css/style-08.css'>";
  echo "<link rel='stylesheet' href='/css/jquery-ui-1.10.4.1.custom.min.css'>";
  echo "<link id='page_favicon' href='/bankrep.ico' rel='icon' type='image/x-icon' />";
  echo "<script src='/js/jquery-1.10.2.min.js'></script>";
  echo "<script src='/js/jquery-ui-1.10.4.custom.min.js'></script>";
  echo "<script src='/js/script-03.min.js'></script>";
  
  
  // Google Analytics
  
  echo "<script>";
  echo "(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){";
  echo "(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),";
  echo "m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)";
  echo "})(window,document,'script','//www.google-analytics.com/analytics.js','ga');";
  
  echo "ga('create', 'UA-58960308-1', 'auto');";
  echo "ga('send', 'pageview');";
  
  echo "</script>";
  
  
  // Yandex.Metrika
  
  echo "<script type='text/javascript'>";
  echo "(function (d, w, c) {";
      echo "(w[c] = w[c] || []).push(function() {";
          echo "try {";
              echo "w.yaCounter28357121 = new Ya.Metrika({id:28357121,";
                      echo "webvisor:true,";
                      echo "clickmap:true,";
                      echo "trackLinks:true,";
                      echo "accurateTrackBounce:true});";
          echo "} catch(e) { }";
      echo "});";

      echo "var n = d.getElementsByTagName('script')[0],";
          echo "s = d.createElement('script'),";
          echo "f = function () { n.parentNode.insertBefore(s, n); };";
      echo "s.type = 'text/javascript';";
      echo "s.async = true;";
      echo "s.src = (d.location.protocol == 'https:' ? 'https:' : 'http:') + '//mc.yandex.ru/metrika/watch.js';";

      echo "if (w.opera == '[object Opera]') {";
          echo "d.addEventListener('DOMContentLoaded', f, false);";
      echo "} else { f(); }";
  echo "})(document, window, 'yandex_metrika_callbacks');";
  echo "</script>";
  echo "<noscript><div><img src='//mc.yandex.ru/watch/28357121' style='position:absolute; left:-9999px;' alt='' /></div></noscript>";

  echo "</head><body>";



  // User input form
  // ---------------

  echo "<div class='input'>";

  echo "<form method='GET' action='index.php'>";

  echo "<input type='hidden' name='m' value='$mode'>";

  echo "<input type='hidden' name='i' value='$bank_id'>";

  echo "<div>";

  echo "<input class='input_bank' type='text' maxlength='50' name='q' value='{$text}' onfocus=\"this.value='';this.style.color='#000000';this.onfocus='';\">";


  // If several banks was found
  // --------------------------

  if($several_banks == true) {
  
    // xx banks found
    // --------------
  
    $number_of_banks = count($banks_results);
  
    if(preg_match('/(\d?\d?[567890]$)|(\d?1\d$)/', $number_of_banks)) {
      $found = "Найдено";
      $banks = "банков";
    } elseif(preg_match('/\d{0,2}[234]$/', $number_of_banks)) {
      $found = "Найдено";
      $banks = "банка";
    } else {
      $found = "Найден";
      $banks = "банк";
    }
  
    echo "<div class='banks'>";
    // echo preg_match('/(\d?\d?[567890]$)|(\d?1\d$)/', $number_of_banks);
    echo "<p>{$found} {$number_of_banks} {$banks}:</p>";
    echo "<ul>";
    foreach($banks_results as $bank) {
      echo "<li><a href='index.php?m={$mode}&q={$bank['id']}{$http_dates_query}{$turnover_query}&s={$multiple}'>{$bank['name']}</a></li>";
    }
    echo "</ul>";
    echo "</div>";
  }


  echo "<span class='bank_name'>{$bank_name}{$message}</span>";

  echo "</div>";



  // Dates input table
  // -----------------

  $current_date = '2017-06-01';

  $months = array(1 => 'янв', 'фев', 'мар', 'апр', 'май', 'июн', 'июл', 'авг', 'сен', 'окт', 'ноя', 'дек');

  echo "<table class='dates'>";

  foreach(range(2011, 2017) as $year) {
  
    echo "<tr>";
  
    // First column (2011 is technical year for printing header row)
    if($year != 2011) {
      echo "<td class='year'>{$year}</td>";
    } else {
      echo "<th></th>";
    }
  
    // Second and the rest columns
    foreach(range(1, 12) as $month) {

      $human_date = mktime(0, 0, 0, $month, 1, $year);
    
      // Header
      if($year == 2011) {
      
        echo "<th class='month'>{$months[$month]}</th>";
      
      // Body
      } else {
      
        // CBR date
        if($month == 12) {
          $month = 1;
          ++$year;
        } else {
          ++$month;
        }
      
        $cbr_date = mktime(0, 0, 0, $month, 1, $year);
      
        if(in_array(date('Y-m-d', $cbr_date), $dates)) {
          $checked = " checked";
        } else {
          $checked = "";
        }
      
        if($human_date <= strtotime($current_date)) {
          echo "<td><input type='checkbox' name='d[]' value='" . date('ym', $human_date) . "'" . $checked . "></td>";
        }
      
      }
    }
  
    echo "</tr>";
  }

  echo "</table>";

  echo "<input class='submit' type='submit' value='Рассчитать'>";


  // Turnover checkbox

  if($turnover_available) {
    echo "<div class='turnover'>С оборотами&nbsp;&nbsp;<input type='checkbox' name='t' value='1'{$turnover}></div>";
  } elseif($turnover_requested) {
    echo "<input type='hidden' name='t' value='1'{$turnover}>";
  }


  // Scale hidden field

  echo "<input type='hidden' name='s' value='{$multiple}'>";


  echo "</form>";

  echo "</div>";



  echo "<div class='mbk'>";
  echo "<a href='http://mbkcentre.pro'>mbkcentre.pro</a>";
  echo "</div>";



  // Sections of financials
  // ----------------------

  echo "<div class='sections'>";
  echo "<ul>";

  foreach($sections as $section => $name) {
    if($section == $mode) {
      echo "<li class='chosen-section'>{$name}</li>";
    } else {
      echo "<li><a href='index.php?m={$section}&q={$bank_id}{$http_dates_query}{$turnover_query}&s={$multiple}'>{$name}</a></li>";
    }
  }

  echo "</ul>";
  echo "</div>";



  // Export to Excel
  // ---------------
  
  if($mode == 101 || $mode == 102 || $mode == 216) {
  
    echo "<div class='export'>";

    echo "<a href='index.php?m={$mode}&q={$bank_id}{$http_dates_query}{$turnover_query}&s={$multiple}&x=1'>Выгрузить в Excel</a>";

    echo "</div>";
  }
}



// Choose section of financials
// ----------------------------

$months = array(1 => 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');

include_once "{$mode}/index.php";



// Footer
// ------

if(!$export) {
  echo "<div class='footer'>";
  echo "<ul>";
  echo "<li>&copy Артем Кулаков</li>";
  echo "</ul>";
  echo "</div>";
}



// Performance
$finish = microtime(true);
// echo $time = round($finish - $start, 1);



// Final HTML
// ----------
if(!$export) {
  echo "</body>";
  echo "</html>";
}
