<?php

// ------------------------------------------------- //
// Test instruction: is there any doubled accounts? //
// ------------------------------------------------- //



// Useful functions
// ----------------
function my_print_r($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}



// Starting HTML
// -------------
echo "<!doctype html>
<html>
<head>
<meta charset='utf-8'>
<title>Doubled_accounts</title>
</head>
<body>\n";



// Get all accounts from Balance Sheet instruction
// -----------------------------------------------
include_once 'pl_instruction.php';

$instruction_accounts = array();
array_walk_recursive($instruction, 'get_account');

// Sub-function 'get_account'
function get_account($account) {
	global $instruction_accounts;
	$instruction_accounts[] = $account;
}

sort($instruction_accounts);



// Is there any doubled accounts?
// ------------------------------
$counter = 0;
$doubled_accounts = array();
foreach($instruction_accounts as $account) {
  ++$counter;
  
  if($counter > 1) {
    if($account == $preceding_account) {
      $doubled_accounts[] = $account;
    }
  }
  
  $preceding_account = $account;
}

my_print_r(array_unique($doubled_accounts));

