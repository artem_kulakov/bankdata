<?php

// ------------------------------------ //
// Automatic comparison with annual P&L //
// ------------------------------------ //



setlocale(LC_ALL,'ru_RU');

$start = microtime(true);



// Useful functions
include_once 'functions.php';



// Master data
// -----------
$title = "P&L comparison";

$dates = array(
	'2014-01-01'
);

$changes = array('Изменение', 'Изменение в %');

// Attach columns 'changes' and '% changes' if user chose more than one dates
/*if(count($dates) > 1) {
  $columns = array_merge($dates, $changes);
} else {
  $columns = $dates;
}*/

$columns = array('2014-01-01', 'cbr', 'difference', 'percent difference');

// my_print_r($columns);

$multiple = 1;



// Reporting errors
// ----------------
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/tmp/php-error.log');



// Define bank id
// --------------
if(isset($_GET['id']) && is_numeric($_GET['id'])) {
  $bank_id = $_GET['id'];
} else {
  $bank_id = 1481;
}



// Connect to MySQL
$before_mysql = microtime(true);

$db = new PDO('mysql:host=localhost;dbname=bankrepr_db', 'bankrepr_user', 'Ybksibkkvmnnp6809463#');
$db->query("SET NAMES UTF8");



// Prepare dates for MySQL query
$dates_for_query = array();

foreach($dates as $date) {
  $dates_for_query[] = "date='{$date}'";
}

$dates_query = implode(' OR ', $dates_for_query);



// Get data from MySQL
// -------------------
$st = $db->query("SELECT form, account, value FROM data WHERE id='{$bank_id}' AND ({$dates_query}) AND (form='102' OR form='pl')");

$results = $st->fetchAll();

/*foreach ($results as $result) {
	$data[$result['date']][$result['account']] = $result['value'];
}*/

foreach ($results as $result) {
  if($result['form'] == '102') {
  	$data[$dates[0]][$result['account']] = $result['value'];
  } else {
    $annual_pl[$result['account']] = $result['value'];
  }
}


// Delete totals and needless lines from $annual_pl
$needless_lines = array(3, '4.1', 5, 14, 15, 16, 18, 20, 22);

foreach($needless_lines as $index) {
  unset($annual_pl[$index]);
}


// Sum up income from securities
$annual_pl[6] = $annual_pl[6] + $annual_pl[7] + $annual_pl[8];
unset($annual_pl[7]);
unset($annual_pl[8]);


// Sort annual BS array and remove associative keys
ksort($annual_pl);
$annual_pl = array_values($annual_pl);

// my_print_r($data);
// my_print_r($annual_pl);

$after_mysql = microtime(true);



// List of accounts
include_once 'accounts_names.php';



// Instruction
include_once 'pl_instruction.php';



// Processing data for Balance Sheet
include_once 'lists.php';



// Attach annual PL to the list of values
// --------------------------------------
$main_lines = array(
  '0-12',
  '0-12-0',
  '0-12-1',
  '0-12-2',
  '0-12-3',
  '0-11',
  '0-11-0',
  '0-11-1',
  '0-11-2',
  '0-10',
  '0-9',
  '0-8',
  '0-7',
  '0-6',
  '0-5',
  '0-4',
  '0-3',
  '0-2',
  '0-1',
  '0-0',
  '0-0-0',
  '0-0-1',
  '0'
);

$main_lines = array_flip($main_lines);

// Look for keys for lines of annual BS
foreach($main_lines as $key => $value) {
  $main_lines[$key] = array_search($key, $id_list);
}

// Attach values of annual BS to $values_list
$i = 0;
foreach($main_lines as $key) {
  $values_list['cbr'][$key] = $annual_pl[$i];
  ++$i;
}

// my_print_r($values_list['cbr']);



// Attach differences to the list of values
// ----------------------------------------
foreach($values_list['cbr'] as $key => $value) {
  $values_list['difference'][$key] = $values_list[$dates[0]][$key] - $value;
  $values_list['percent difference'][$key] = round((($values_list[$dates[0]][$key] / $value - 1) * 100), 1);
}

// my_print_r($values_list['difference']);
// my_print_r($values_list['percent difference']);



// Starting HTML
// -------------
echo "<!doctype html><html><head>";
echo "<meta charset='utf-8'>";
echo "<title>{$title}</title>";
echo "<link rel='stylesheet' href='css/style-03.css'>";
echo "<link rel='stylesheet' href='jquery-ui-1.10.4.1.custom.css'>";
echo "<script src='jquery-1.10.2.min.js'></script>";
echo "<script src='jquery-ui-1.10.4.custom.min.js'></script>";
echo "<script src='script-01.js'></script>";
echo "</head><body>";



// Registration number input
// -------------------------
echo "<form method='GET' action='comparison.php'>";
echo "<input type='text' maxlength='4' name='id' value='{$bank_id}'>";
echo "<input type='submit' value='Поехали!'>";
echo "</form>";




// Function print_values()
include_once 'print_values.php';



// Result table
// ------------
krsort($id_list);

$before_html = microtime(true);

echo "<table class='result'>";

// Header
echo "<tr><th>Наименование статьи</th>";
foreach($dates as $date) {
	echo "<th>" . strftime("%e %B %Y", strtotime($date)) . "</th>";
}
// Changes columns if user shose more than one date
if(count($dates) > 1) {
  foreach($changes as $item) {
    echo "<th>{$item}</th>";
  }
}
echo "</tr>";

// Body
foreach($id_list as $index => $id) {
	if(preg_match('/^\d(-\d{1,2})?$/', $id)) {
		echo "<tr id='{$id}'{$classes_list[$index]}>";
		
		echo "<td><span class='ui-icon ui-icon-triangle-1-e'></span>{$names_list[$index]}</td>";
    print_values();
	}
}

echo "</table>";



// Source table
// ------------
echo "<table class='source'>";

// Body
foreach($id_list as $index => $id) {
	echo "<tr id='{$id}'{$classes_list[$index]}>";
	
	// Choosing icon at the beginning of the line
	if(in_array($index, $indexes_of_accounts)) {
		$span = "<span class='ui-icon ui-icon-blank'></span>";
	} else {
		$span = "<span class='ui-icon ui-icon-triangle-1-e'></span>";
	}
	
	echo "<td><div>{$span}{$names_list[$index]}</div></td>";
	print_values();
}
echo "</table>";

$after_html = microtime(true);



// Check PL accuracy
// -----------------
foreach($dates as $date) {
  echo $values_list[$date]['0'] - ($data[$date]['33001'] + $data[$date]['33002']) . "<br>";
}

echo "<br>";



// Memory usage
// ------------
echo "Peak memory: " . round(memory_get_peak_usage(true)/1024/1024, 1) . " MB<br>";
echo "Memory: " . round(memory_get_usage(true)/1024/1024, 1) . " MB<br><br>";



// Meter performance
// -----------------
$finish = microtime(true);

$total = round($finish - $start, 2);
echo "Total: $total seconds<br>";

$mysql = round($after_mysql - $before_mysql, 2);
echo "&nbsp;&nbsp;MySQL: $mysql seconds<br>";

$lists = round($after_lists - $before_lists, 2);
echo "&nbsp;&nbsp;Lists: $lists seconds<br>";

$ids = round($after_ids - $before_ids, 2);
echo "&nbsp;&nbsp;&nbsp;&nbsp;IDs: $ids seconds<br>";

$values = round($after_values - $before_values, 2);
echo "&nbsp;&nbsp;&nbsp;&nbsp;Values: $values seconds<br>";

$html_duration = round($after_html - $before_html, 2);
echo "&nbsp;&nbsp;HTML: $html_duration seconds<br>";



// Final HTML
// ----------
echo "</body>";
echo "</html>";
