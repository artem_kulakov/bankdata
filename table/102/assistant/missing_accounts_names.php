<?php

// ------------------------------------------------------------------------- //
// Test instruction: what accounts are not used in instruction //
// ------------------------------------------------------------------------- //



// Useful functions
// ----------------
function my_print_r($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}



$accounts = array(10001, 10002, 10003, 17307, 20001, 20002, 20003, 27309, 28201, 28202, 31001, 31002, 32101, 33001, 33002, 40001, 40011, 40012, 40014, 40015, 40017, 40018, 40019, 40110, 40111, 40112, 40113, 40114, 40115, 40116, 40117, 40118, 40119);



include_once 'accounts_names.php';



foreach($accounts as $account) {
	echo "{$account}: {$accounts_names[$account]}<br>";
}