<?php

// ---------------------------------------------- //
// Automatic comparison with annual Balance Sheet //
// ---------------------------------------------- //



setlocale(LC_ALL,'ru_RU');

$start = microtime(true);



// Master data
// -----------
$bank_id = 1481;

$dates = array('2014-01-01');
$columns = array('2014-01-01', 'cbr', 'difference', 'percent difference');

$multiple = 1;



// Useful functions
// ----------------
function my_print_r($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}



// Reporting errors
// ----------------
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/tmp/php-error.log');



// Connect to MySQL
$before_mysql = microtime(true);

$db = new PDO('mysql:host=localhost;dbname=bankrepr_db', 'bankrepr_user', 'Ybksibkkvmnnp6809463#');
$db->query("SET NAMES UTF8");



// Get data from MySQL
// -------------------
$st = $db->query("SELECT form, account, value FROM data WHERE id='{$bank_id}' AND date='{$dates[0]}' AND (form='101' OR form='bs')");

$results = $st->fetchAll();

foreach ($results as $result) {
  if($result['form'] == '101') {
  	$data[$dates[0]][$result['account']] = $result['value'];
  } else {
    $annual_bs[$result['account']] = $result['value'];
  }
}

// Sort annual BS array and remove associative keys
ksort($annual_bs);
$annual_bs = array_values($annual_bs);

// my_print_r($data);
// my_print_r($annual_bs);

$after_mysql = microtime(true);



// List of accounts
include_once 'accounts_names.php';



// Instruction
include_once 'instruction.php';



// Provisions on overdue loans
// ---------------------------
$overdue_loans = array();

foreach($dates as $date) {
	// Calculate overdue corporate loans
	foreach($instruction['Активы']['Кредиты и авансы клиентам']['Кредиты юридическим лицам']['Просроченные кредиты'] as $account) {
		$overdue_loans[$date]['corporate'] += $data[$date][$account];
	}
	
	// Calculate overdue retail loans
	foreach($instruction['Активы']['Кредиты и авансы клиентам']['Кредиты физическим лицам']['Просроченные кредиты'] as $account) {
		$overdue_loans[$date]['retail'] += $data[$date][$account];
	}
	
	// Divide provisions between corporate and retail, and attach to MySQL data
	$data[$date]['45818/Ю'] = $data[$date][45818] / ($overdue_loans[$date]['corporate'] + $overdue_loans[$date]['retail']) * $overdue_loans[$date]['corporate'];
	$data[$date]['45818/Ф'] = $data[$date][45818] / ($overdue_loans[$date]['corporate'] + $overdue_loans[$date]['retail']) * $overdue_loans[$date]['retail'];
}

// my_print_r($overdue_loans);



// List of IDs
// -----------
$before_lists = microtime(true);
$before_ids = microtime(true);

function get_id($instruction, $parent_id) {
	global $id_list;
	$counter = 0;
	foreach($instruction as $item) {
		$id = $parent_id . '-' . $counter;
		$id_list[] = $id;
		++$counter;
		if(is_array($item)) {
			get_id($item, $id);
		}
	}
}

// Calling get_id function
$id_list = array();
get_id($instruction, '');

// Removing leading "-" from ID
foreach($id_list as $key => $item) {
	$id_list[$key] = substr($item, 1);
}

$after_ids = microtime(true);

// my_print_r($id_list);



// List of values
// --------------
$before_values = microtime(true);

function get_values($instruction) {
	global $sum, $date, $one_date_data, $values_list;
	foreach($instruction as $key => $item) {
		$sum = 0;
		if(is_array($item)) {
			array_walk_recursive($item, 'get_sum');
			$values_list[$date][] = $sum;
			get_values($item);
		} else {
			$values_list[$date][] = $one_date_data[$item];
		}
	}
}

// Sub-function 'get_sum'
function get_sum($value, $key) {
	global $sum, $date, $one_date_data;
	$sum += $one_date_data[$value];
}

// Calling get_values function for each date
foreach($data as $date => $one_date_data) {
	$values_list[$date] = array();
	get_values($instruction);
}

// my_print_r($values_list);

$after_values = microtime(true);



// List of names and indexes of lines with accounts
// ------------------------------------------------
function get_names($instruction) {
	global $names_list, $accounts_names, $indexes_of_accounts;
	foreach($instruction as $key => $item) {
		if(is_array($item)) {
			$names_list[] = $key;
			get_names($item);
		} else {
			$names_list[] = $item . " " . $accounts_names[$item];
			
			// Get indexes of lines with accounts
			$indexes_of_accounts[] = count($names_list)-1;
		}
	}
}

// Calling get_names function
$names_list = array();
$indexes_of_accounts = array();
get_names($instruction);

// my_print_r($names_list);
// my_print_r($indexes_of_accounts);



// List of classes
// ---------------
$classes_list = array();
foreach($id_list as $key => $item) {
	$number = substr_count($item, '-');
	if($number > 0) {
		if(in_array($key, $indexes_of_accounts)) {
			$classes_list[] = " class='account x" . $number . "0'";
		} else {
			$classes_list[] = " class='x" . $number . "0'";
		}
	} else {
		$classes_list[] = "";
	}
}

// my_print_r($classes_list);



/*// Remove empty and zero lines
// ---------------------------
foreach($id_list as $index => $id) {
	$line_is_empty = true;
	
	foreach($dates as $date) {
		$this_value = $values_list[$date][$index];
		if($this_value != 0 && $this_value != '') {
			$line_is_empty = false;
		}
	}
	
	if($line_is_empty == true) {
		unset($id_list[$index]);
	}
}*/

$after_lists = microtime(true);



// Attach annual BS to the list of values
// --------------------------------------
$main_lines = array(
  '0-0', '0-1', '0-1-0', '0-2', '0-3', '0-4', '0-5', '0-5-0', '0-6', '0-7', '0-8', 0,
  '1-0', '1-1', '1-2', '1-2-0', '1-3', '1-4', '1-5', '1-6', 1,
  '2-0', '2-1', '2-2', '2-3', '2-4', '2-5', '2-6', '2-7', 2,
  '3-0', '3-1', '3-2'
);

$main_lines = array_flip($main_lines);

// Look for keys for lines of annual BS
foreach($main_lines as $key => $value) {
  $main_lines[$key] = array_search($key, $id_list);
}

// Attach values of annual BS to $values_list
$i = 0;
foreach($main_lines as $key) {
  $values_list['cbr'][$key] = $annual_bs[$i];
  ++$i;
}

// my_print_r($values_list['cbr']);



// Attach differences to the list of values
// ----------------------------------------
foreach($values_list['cbr'] as $key => $value) {
  $values_list['difference'][$key] = $values_list[$dates[0]][$key] - $value;
  $values_list['percent difference'][$key] = round((($values_list[$dates[0]][$key] / $value - 1) * 100), 1);
}

// my_print_r($values_list['difference']);
// my_print_r($values_list['difference']);



// Starting HTML
// -------------
echo "<!doctype html>
<html>
<head>
<meta charset='utf-8'>
<title>Table - Comparison</title>
<link rel='stylesheet' href='css/style-03.css'>
<link rel='stylesheet' href='jquery-ui-1.10.4.1.custom.css'>
<script src='jquery-1.10.2.min.js'></script>
<script src='jquery-ui-1.10.4.custom.min.js'></script>
<script src='script-01.js'></script>
</head>
<body>\n";



// Header
// ------
echo "<h1>{$bank_id}, " . strftime("%e %B %Y", strtotime($date)) . "</h1>";



// Result table
// ------------
echo "<table class='result'>\n";

// Header
echo "<tr><th>Наименование статьи</th>";
echo "<th>bankrep.ru</th>";
echo "<th>cbr.ru</th>";
echo "<th>Разница</th>";
echo "<th>Разница в %</th></tr>";

// Body
foreach($id_list as $index => $id) {
	if(is_numeric($id)) {
		echo "<tr id='{$id}'{$classes_list[$index]}>";
		
		echo "<td><span class='ui-icon ui-icon-triangle-1-e'></span>{$names_list[$index]}</td>";
    print_values();
	}
}

echo "</table>\n";



// Source table
// ------------
echo "<table class='source'>\n";

// Body
foreach($id_list as $index => $id) {
	echo "<tr id='{$id}'{$classes_list[$index]}>";
	
	// Choosing icon at the beginning of the line
	if(in_array($index, $indexes_of_accounts)) {
		$span = "<span class='ui-icon ui-icon-blank'></span>";
	} else {
		$span = "<span class='ui-icon ui-icon-triangle-1-e'></span>";
	}
	
	echo "<td><div>{$span}{$names_list[$index]}</div></td>";
	print_values();
}
echo "</table>\n";



// Function printing table values
// ------------------------------
function print_values() {
  global $id, $columns, $values_list, $multiple, $index;
  
  // Sign of value
  if(substr($id, 0, 1) == 1 || substr($id, 0, 1) == 2 || substr($id, 0, 1) == 3) {
    $sign = -1;
  } else {
    $sign = 1;
  }
  
  // Value
  foreach($columns as $date) {
    
    // Number of decimals; $multiple
    if($date == 'percent difference') {
      $decimals = 1;
      $mult = 1;
    } else {
      $decimals = 0;
      $mult = $multiple;
    }
    
    // Format value
    $value = number_format($values_list[$date][$index] / $mult * $sign, $decimals, ',', ' ');
    
    // Show zero values as '-'
    if($value == '0' || $value == '0,0') {
      $value = "-";
    }
    
    echo "<td>{$value}</td>";
  }
  
  // Closing tr
  echo "</tr>\n";
}



// Memory usage
// ------------
echo "Peak memory: " . round(memory_get_peak_usage(true)/1024/1024, 1) . " MB<br>";
echo "Memory: " . round(memory_get_usage(true)/1024/1024, 1) . " MB<br><br>";



// Meter performance
// -----------------
$finish = microtime(true);

$total = round($finish - $start, 2);
echo "Total: $total seconds<br>";

$mysql = round($after_mysql - $before_mysql, 2);
echo "&nbsp;&nbsp;MySQL: $mysql seconds<br>";

$lists = round($after_lists - $before_lists, 2);
echo "&nbsp;&nbsp;Lists: $lists seconds<br>";

$ids = round($after_ids - $before_ids, 2);
echo "&nbsp;&nbsp;&nbsp;&nbsp;IDs: $ids seconds<br>";

$values = round($after_values - $before_values, 2);
echo "&nbsp;&nbsp;&nbsp;&nbsp;Values: $values seconds<br>";



// Final HTML
// ----------
echo "</body>\n";
echo "</html>\n";
