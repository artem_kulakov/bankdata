<?php

// ------------------------------------------------- //
// Test instruction: is there any doubled accounts? //
// ------------------------------------------------- //



// Useful functions
// ----------------
function my_print_r($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}



// Starting HTML
// -------------
echo "<!doctype html>
<html>
<head>
<meta charset='utf-8'>
<title>Doubled_accounts</title>
</head>
<body>\n";



// Get all accounts from Balance Sheet instruction
// -----------------------------------------------
include_once '../instruction.php';

$instruction_accounts = array();
array_walk_recursive($instruction, 'get_account');

// Sub-function 'get_account'
function get_account($account) {
	global $instruction_accounts;
	$instruction_accounts[] = $account;
}

sort($instruction_accounts);



// Is there any doubled accounts?
// ------------------------------
$counter = 0;
$doubled_accounts = array();
foreach($instruction_accounts as $account) {
  ++$counter;
  
  if($counter > 1) {
    if($account == $preceding_account) {
      $doubled_accounts[] = $account;
    }
  }
  
  $preceding_account = $account;
}

my_print_r($doubled_accounts);



/*// Connect to MySQL
$db = new PDO('mysql:host=localhost;dbname=bankrepr_db', 'bankrepr_user', 'Ybksibkkvmnnp6809463#');
$db->query("SET NAMES UTF8");



// Get doubled accounts with values from MySQL
// -------------------------------------------
$accounts_query = "";
foreach($doubled_accounts as $account) {
  $accounts_query .= "account={$account} OR ";
}
$accounts_query = substr($accounts_query, 0, -4);

$st = $db->query("SELECT account, value FROM data WHERE form=101 AND ($accounts_query) GROUP BY account ORDER BY account ASC");
$results = $st->fetchAll();
foreach ($results as $result) {
	$accounts_and_values[$result['account']] = $result['value'];
}

my_print_r($accounts_and_values);*/
