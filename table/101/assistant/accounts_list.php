<?php

// --------------------------------------------------------------------------------------- //
// Get the list of accounts (like 10201, 10202, etc.) of certain parent account (like 102) //
// --------------------------------------------------------------------------------------- //



// Useful function
// ----------------
function my_print_r($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}



// Get all accounts from Balance Sheet instruction
// -----------------------------------------------
include_once '../instruction_kuap.php';

$instruction_accounts = array();
array_walk_recursive($instruction, 'get_account');

// Sub-function 'get_account'
function get_account($account) {
	global $instruction_accounts;
	$parent_account = substr($account, 0, 3);
	$instruction_accounts[$parent_account][] = $account;
}

// my_print_r($instruction_accounts);



// Print lists of accounts
ksort($instruction_accounts);
foreach($instruction_accounts as $parent_account) {
  sort($parent_account);
  echo implode(', ', $parent_account) . "<br>";
}
