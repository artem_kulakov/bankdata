<?php

// ---------------------------------------------------------- //
// Get names of certain accounts for insertion to instruction //
// ---------------------------------------------------------- //



// Master data
$parent_account = 454;


// Useful function
// ---------------
function my_print_r($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}



// Date
/*$report_date = '2014-09-01';
$rar_date = date("Ym", strtotime($report_date));



// Download and open RAR archive
file_put_contents("101-{$rar_date}01.rar", fopen("http://cbr.ru/credit/forms/101-{$rar_date}01.rar", 'r'));
$rar_file = rar_open("101-{$rar_date}01.rar");



// Extract DBF file
$entry = rar_entry_get($rar_file, 'NAMES.DBF');
$entry->extract(".");



// Close RAR archive
rar_close($rar_file);*/



// Starting HTML
// -------------
echo "<!doctype html>
<html>
<head>
<meta charset='utf-8'>
<title>Certain Accounts Names</title>
</head>
<body>\n";



// Get accounts names
// ------------------
$dbf = dbase_open('NAMES.DBF', 0);
$num_records = dbase_numrecords($dbf);
// $num_records = 30;

for($i = 1; $i <= $num_records; ++$i) {
  
  $record = dbase_get_record($dbf, $i);
  
  $account = preg_replace("/\x20/", "", $record[1]);
  
  $account_name = $record[2];
  
  // Show only parent accounts
  if(substr($account, 0, 3) == $parent_account) {
    $string = "'{$account_name}' => array(<br>";
    $string = iconv("cp866", "UTF-8", $string);
    echo $string;
  }
}



// Final HTML
echo "</body>\n";
echo "</html>\n";
