<?php


// Last and second last dates
// --------------------------

$last_date = end($dates);
$second_last_date = prev($dates);



// Insert 'dr' and 'cr' to $dates
// ------------------------------

if($turnover_available && $turnover_requested && $few_dates) {
  $temp = array_pop($dates);
  array_push($dates, 'dr', 'cr', $temp);
}

// my_print_r($dates);



// List of accounts
include_once 'accounts_names.php';



// Instruction
include_once 'instruction.php';



// Processing data
// ---------------

if(!$export) {
  require_once 'lists.php';
} else {
  require_once 'lists_export.php';
}



// Attach totals
// -------------

$indexes_of_totals = array();

foreach($totals as $name => $lines) {
  
  // Index of total
  $parent_index = array_search($name, $names_list);
  $indexes_of_totals[] = $parent_index;
  
  // Index of items of total
  $child_indexes = array();
  foreach($lines as $line) {
    $child_indexes[] = array_search($line, $names_list);
  }
  
  foreach($dates as $date) {
    
    // Value of total
    $sum = 0;
    foreach($child_indexes as $index) {
      $sum += $values_list[$date][$index];
    }
    
    // Attach value to total
    $values_list[$date][$parent_index] = $sum;
    
  }
  
  // Attach class
  $classes_list[$parent_index] = " class='total'";
  
}

// my_print_r($indexes_of_totals);
// my_print_r($values_list);



// Attach columns 'changes' and '% changes' if user chose more than one dates
// --------------------------------------------------------------------------

$changes = array('Изменение', 'Изменение в %');

if($few_dates) {
  $columns = array_merge($dates, $changes);
} else {
  $columns = $dates;
}

// my_print_r($columns);

// $multiple = 1;



// Attach changes to the list of values
// ------------------------------------
if($few_dates) {
  
  // Calculate changes and attach to the list of values
  foreach($id_list as $key => $id) {
    $values_list[$changes[0]][$key] = $values_list[$last_date][$key] - $values_list[$second_last_date][$key];
  
    // Avoid division by zero (the result becomes -100,0%)
    if($values_list[$second_last_date][$key] != 0) {
      $values_list[$changes[1]][$key] = round((($values_list[$last_date][$key] / $values_list[$second_last_date][$key] - 1) * 100), 1);
    } else {
      $values_list[$changes[1]][$key] = 0;
    }
  }
  
}

// my_print_r($values_list);
// my_print_r($values_list[$changes[1]]);



// Function print_values()
include_once 'print_values.php';



// Result table
// ------------

if(!$export) {

  echo "<div id='_101'>";

  echo "<table class='result'>";


  // Header

  echo "<tr><th>";


  // Scale

  echo "<select class='scale' onchange='if (this.value) window.location.href=this.value'>";

  foreach($scale as $key => $value){
    if($key == $multiple) {
      $selected = " selected";
    } else {
      $selected = "";
    }

    echo "<option value='index.php?m={$mode}&q={$bank_id}{$http_dates_query}{$turnover_query}&s={$key}'{$selected}>{$value}</option>";
  }

  echo "</select>";


  echo "</th>";

  foreach($dates as $date) {
    $year  = substr($date, 0, 4);
    $month = intval(substr($date, 5, 2));
  
    if($date == 'dr') {
      echo "<th class='secondary'>Обороты по дебету</th>";
    } elseif($date == 'cr') {
      echo "<th class='secondary'>Обороты по кредиту</th>";
    } else {
      echo "<th>1 {$months[$month]} {$year}</th>";
    }
  }
  // Changes columns if user chose more than one date
  if($few_dates) {
    foreach($changes as $item) {
      echo "<th class='secondary'>{$item}</th>";
    }
  }
  echo "</tr>";

  // Body
  foreach($id_list as $index => $id) {
    if(preg_match('/^\d{1,2}$/', $id)) {
      echo "<tr id='{$id}'{$classes_list[$index]}>";
    
      // Choosing icon at the beginning of the line
      if(in_array($index, $indexes_of_totals)) {
        $span = "<span class='ui-icon ui-icon-blank'></span>";
      } else {
        $span = "<span class='ui-icon ui-icon-triangle-1-e'></span>";
      }
    
      echo "<td>{$span}{$names_list[$index]}</td>";
      print_values();
    }
  }

  echo "</table>";
  
  
  
  // Source table
  // ------------
  echo "<table class='source'>";

  // Body
  foreach($id_list as $index => $id) {
    echo "<tr id='{$id}'{$classes_list[$index]}>";
  
    // Choosing icon at the beginning of the line
    if(in_array($index, $indexes_of_accounts)) {
      $span = "<span class='ui-icon ui-icon-blank'></span>";
    } else {
      $span = "<span class='ui-icon ui-icon-triangle-1-e'></span>";
    }
  
    echo "<td><div>{$span}<div class='line_name'>{$names_list[$index]}</div></div></td>";
    print_values();
  }
  echo "</table>";

  echo "</div>";
  
  
// Export to Excel

} else {
  
  // Header
  
  // Bank name
  $exported['name'][] = "{$bank_name}, {$bank_id}";
  $exported['blank_1'][] = '';
  
  // Scale
  $exported['scale'][] = $scale[$multiple];
  $exported['blank_2'][] = '';
  
  $exported['header'][] = "Раздел";
  $exported['header'][] = "Подраздел";
  
  foreach($dates as $date) {
    $year  = substr($date, 0, 4);
    $month = intval(substr($date, 5, 2));
  
    if($date == 'dr') {
      $exported['header'][] = "Обороты по дебету";
    } elseif($date == 'cr') {
      $exported['header'][] = "Обороты по кредиту";
    } else {
      $exported['header'][] = "1 {$months[$month]} {$year}";
    }
  }
  
  // Changes columns if user chose more than one date
  if($few_dates) {
    foreach($changes as $item) {
      $exported['header'][] = $item;
    }
  }
  
  
  // Names and values
  
  foreach($id_list as $index => $id) {
    $exported[$index][] = $names_list[$index];
    $exported[$index][] = $names_list_2[$index];
    print_values();
  }
  
  include_once 'excel.php';
  
}

// my_print_r($exported);



// Check BS accuracy
// -----------------

/*foreach($dates as $date) {
  $sum = 0;
  
  foreach($indexes_of_totals as $index) {
    $sum += $values_list[$date][$index];
  }
  
  echo round($sum, 0) . "<br>";
}*/
