<?php

// ------------------ //
// Print table values //
// ------------------ //



function print_values() {
  global $id, $changes, $columns, $values_list, $multiple, $index, $indexes_of_totals, $export, $exported;
  
  // Sign of value
  if($index > $indexes_of_totals[0]) {
    $sign = -1;
  } else {
    $sign = 1;
  }
  
  // Value
  foreach($columns as $date) {
    
    $class = " class='secondary'";
    
    // Output parameters for turnover
    if($date == 'dr' || $date == 'cr') {
      $decimals = 0;
      $mult = $multiple;
      $unit = "";
      
      // Positive sign for positive turnover
      if(($index <= $indexes_of_totals[0] && $values_list[$date][$index] > 0) || ($index > $indexes_of_totals[0] && $values_list[$date][$index] < 0)) {
        $plus = "+";
      } else {
        $plus = "";
      }
      
    // Output parameters for change
    } elseif($date == $changes[0]) {
      $decimals = 0;
      $mult = $multiple;
      $unit = "";
      
      // Positive sign for positive change
      if(($index <= $indexes_of_totals[0] && $values_list[$date][$index] > 0) || ($index > $indexes_of_totals[0] && $values_list[$date][$index] < 0)) {
        $plus = "+";
      } else {
        $plus = "";
      }
      
    // Output parameters for % change
    } elseif($date == $changes[1]) {
      $decimals = 1;
      $mult = 1;
      $unit = "%";
      $sign = 1;
      
      // Positive sign for positive change
      if($values_list[$date][$index] > 0) {
        $plus = "+";
      } else {
        $plus = "";
      }
      
    } else {
      $decimals = 0;
      $mult = $multiple;
      $unit = "";
      $plus = "";
      
      $class = "";
    }
    
    // Format value
    $value = $plus . number_format($values_list[$date][$index] / $mult * $sign, $decimals, ',', ' ') . $unit;
    
    // Show zero values as '-'
    if($value == '0' || $value == '0,0%') {
      $value = "-";
    }
    
    if(!$export) {
      echo "<td{$class}>{$value}</td>";
    } else {
      $exported[$index][] = round(($values_list[$date][$index] / $mult * $sign), $decimals) . $unit;
    }
  }
  
  // Closing tr
  if(!$exported) {
    echo "</tr>";
  }
}
