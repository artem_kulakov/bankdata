<?php

// Attach columns 'changes' and '% changes' if user chose more than one dates
// --------------------------------------------------------------------------

$changes = array('Изменение');

if(count($dates) > 1) {
  $columns = array_merge($dates, $changes);
} else {
  $columns = $dates;
}

// my_print_r($columns);



// List of accounts
include_once 'accounts_names.php';



// Function print_values()
include_once 'print_values.php';



// Rename N1 to N1.0
foreach($dates as $date) {
  foreach($data[$date] as $ratio => $value) {
    if($ratio === 1) {
      $data[$date]['1.0'] = $value;
      unset($data[$date][1]);
    }
  }
}



// Result table
// ------------

echo "<div id='_135'>";

echo "<table class='result'>";

// Header
echo "<tr><th>В процентах</th><th>Допустимое значение</th>";
foreach($dates as $date) {
  $year  = substr($date, 0, 4);
  $month = intval(substr($date, 5, 2));
  
	echo "<th>1 {$months[$month]} {$year}</th>";
}

// Changes columns if user chose more than one date
if(count($dates) > 1) {
  echo "<th>Изменение</th>";
}

echo "</tr>";


// Body
$ratios = array(
  '1.0'   => array('Мин.', 8),
  '1.1'   => array('Мин.', 4.5),
  '1.2'   => array('Мин.', 5.5),
  '2'     => array('Мин.', 15),
  '3'     => array('Мин.', 50),
  '4'     => array('Макс.', 120),
  '7'     => array('Макс.', 800),
  '10.1'  => array('Макс.', 3),
  '12'    => array('Макс.', 25)
);


// Last and second last dates
$last_date = end($dates);
$second_last_date = prev($dates);


foreach($ratios as $ratio => $condition) {
  echo "<tr>";
  
  echo "<td>{$accounts_names[$ratio]} (Н{$ratio})</td>";
  
  echo "<td>{$condition[0]} {$condition[1]}</td>";
  
  foreach($dates as $date) {
    
    
    // New ratios 1.1 and 1.2 starting from 2014-02-01
    if(($ratio === '1.1' || $ratio === '1.2') && strtotime($date) < strtotime('2014-02-01')) {
      $value = "-";
    } else {
      $value = $data[$date][$ratio];
    }
    
    // New limit for 1.2 starting from 2015-01-01
    if(($ratio === '1.2') && strtotime($date) < strtotime('2015-01-01')) {
      $condition[1] = 5.5;
    }
    
    // Breached or not?
    if(($condition[0] == 'Мин.' && $value < $condition[1]) || ($condition[0] == 'Макс.' && $value > $condition[1])) {
      $class = " class='breached'";
    } else {
      $class = "";
    }
    
    echo "<td{$class}>" . number_format($value, 2, ",", "") . "</td>";
  }
  
  // Change
  if(count($dates) > 1) {
    $change = $data[$last_date][$ratio] - $data[$second_last_date][$ratio];
    
    // Positive sign for positive change
    if($change > 0) {
      $plus = "+";
    } else {
      $plus = "";
    }
    
    echo "<td>" . $plus . number_format($change, 2, ",", "") . "</td>";
  }
  
  echo "</tr>";
}

echo "</table>";

echo "</div>";
