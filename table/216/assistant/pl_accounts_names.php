<?php

// --------------------------------------- //
// Get names of P&L accounts from DBF file //
// --------------------------------------- //



// Useful function
// ---------------
function my_print_r($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}



// Date
$report_date = '2014-10-01';
$rar_date = date("Ym", strtotime($report_date));



// Download and open RAR archive
/*file_put_contents("102-{$rar_date}01.rar", fopen("http://cbr.ru/credit/forms/102-{$rar_date}01.rar", 'r'));
$rar_file = rar_open("102-{$rar_date}01.rar");



// Extract DBF file
$entry = rar_entry_get($rar_file, 'SPRAV1.DBF');
$entry->extract(".");



// Close RAR archive
rar_close($rar_file);*/



// Starting HTML
// -------------
echo "<!doctype html>
<html>
<head>
<meta charset='utf-8'>
<title>P&L Accounts Names</title>
</head>
<body>\n";



// Get accounts names
$dbf = dbase_open('SPRAV1.DBF', 0);
$num_records = dbase_numrecords($dbf);
// $num_records = 30;

for($i = 1; $i <= $num_records; ++$i) {
  
  $record = dbase_get_record($dbf, $i);
  
  $account = preg_replace("/\x20/", "", $record[2]);
  
  $account_name = $record[3];
  
  $account_and_name = "'{$account}' => '{$account_name}',<br>";
  $account_and_name = iconv("cp866", "UTF-8", $account_and_name);
  echo $account_and_name;
}



// Final HTML
echo "</body>\n";
echo "</html>\n";
