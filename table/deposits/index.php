<?php


// Master data
// -----------

$basket = array(
  '2014-11-01' => 46.8043,
  '2014-12-01' => 54.7620
);

$dates = array(
  '2014-11-01',
  '2014-12-01'
);

$start_date = $dates[0];
$end_date   = $dates[1];

$lost_licenses = array(
  '2014-12-01' => array(2922, 1684, 625, 653, 3449, 1943)
);

$liquidated = array(
  '2014-12-01' => array(1950, 2179, 2786)
);



// Useful function
// ---------------
function my_print_r($var) {
	echo "<pre>";
	print_r($var);
	echo "</pre>";
}



// Reporting errors
// ----------------
ini_set('error_reporting', E_ALL | E_STRICT);
ini_set('display_errors', 'Off');
ini_set('log_errors', 'On');
ini_set('error_log', '/tmp/php-error.log');



// BS instruction
include_once '../101/instruction.php';



// Convert branch of instruction to simple array of accounts
// ---------------------------------------------------------

get_accounts($instruction['Средства клиентов']['Средства физических лиц']);

function get_accounts($array) {
  
  global $accounts;
  
  foreach($array as $item) {
    
    if(is_array($item)) {
      get_accounts($item);
    } else {
      $accounts[] = $item;
    }
  }
  
  // return $total;
}

// my_print_r($accounts);



// Connect to MySQL
// ----------------
$before_mysql = microtime(true);

$db = new PDO('mysql:host=localhost;dbname=bankrepr_db', 'bankrepr_user', 'Ybksibkkvmnnp6809463#');
$db->query("SET NAMES UTF8");



// Get bank names from MySQL
// -------------------------
$st = $db->query("SELECT id, name FROM banks");
$results = $st->fetchAll();

foreach($results as $bank) {
  $banks_names[$bank['id']] = $bank['name'];
}

// my_print_r($banks_names);



// Prepare dates for MySQL query
// -----------------------------

$dates_for_query = array();

foreach($dates as $date) {
  $dates_for_query[] = "date='{$date}'";
}

$dates_query = implode(' OR ', $dates_for_query);



// Prepare accounts for MySQL query
// -----------------------------

$accounts_for_query = array();

foreach($accounts as $account) {
  $accounts_for_query[] = "account='{$account}'";
}

$accounts_query = implode(' OR ', $accounts_for_query);



// Get numbers from MySQL
// ----------------------
$st = $db->query("SELECT id, date, account, rub, fx, total FROM data WHERE ({$dates_query}) AND ({$accounts_query}) AND form='101'");
$results = $st->fetchAll();

// my_print_r($results);



// Sum up retail deposits
// ----------------------

// foreach($dates as $date) {
//   $sum[$date] = 0;
// }

$currencies = array('rub', 'fx', 'total');

foreach($results as $result) {
  
  // Bank system sum
	$sum[$result['date']] += $result['total'];
	
	// Each bank
	foreach($currencies as $currency) {
    if(!isset($bank_sum[$result['id']][$result['date']][$currency])) {
      $bank_sum[$result['id']][$result['date']][$currency] = 0;
    }
    
    $bank_sum[$result['id']][$result['date']][$currency]   -= $result[$currency];
  }
	
}



// Alfa-Bank
// ---------

$fx_share['2014-11-01'] = 0.32;
$fx_share['2014-12-01'] = 0.355088;


// FX basket change
$basket_change = $basket[$end_date] / $basket[$start_date];


// Split Total to RUB and FX as of 1 November 2014 based on public data
$bank_sum[1326][$start_date]['rub'] = round($bank_sum[1326][$start_date]['total'] * (1 - $fx_share[$start_date]), 0);
$bank_sum[1326][$start_date]['fx'] = round($bank_sum[1326][$start_date]['total'] * $fx_share[$start_date], 0);


// Split Total to RUB and FX as of 1 December 2014 assuming stable RUB/FX mix
$rub_share = (1 - $fx_share[$start_date]);
$fx_share = $fx_share[$start_date] * $basket_change;
$total_share = $rub_share + $fx_share;

$rub_share = $rub_share / $total_share;
$fx_share = $fx_share / $total_share;

$bank_sum[1326][$end_date]['rub'] = round($bank_sum[1326][$end_date]['total'] * $rub_share, 0);
$bank_sum[1326][$end_date]['fx'] = round($bank_sum[1326][$end_date]['total'] * $fx_share, 0);



// Revalue later date numbers
// --------------------------

foreach($bank_sum as $id => $values) {
  if(isset($bank_sum[$id][$end_date])) {
    $bank_sum[$id][$end_date]['fx_adj'] = round(($values[$end_date]['fx'] / $basket_change), 0);
    $bank_sum[$id][$end_date]['total_adj'] = $bank_sum[$id][$end_date]['rub'] + $bank_sum[$id][$end_date]['fx_adj'];
  }
}

// my_print_r($bank_sum[1037]);
// my_print_r($bank_sum[1481]);



// Change for whole banking system
// -------------------------------

/*echo "Вся банковская система:<br>";

// Print retail deposits for whole banking system for each date
foreach($dates as $date) {
  echo "{$date}: " . number_format(-$sum[$date]/1000000, 0, ',', ' ') . " млрд рублей<br>";
}

// Change in total retail deposits
$change = $sum[$end_date] - $sum[$start_date];

if($change < 0) {
  $sign = "+";
} else {
  $sign = "";
}

// Print change of total retail deposits
echo "Изменение: " . $sign . number_format(-$change/1000000, 0, ',', ' ') . " млрд рублей<br>";*/



// Change for each bank
// --------------------

foreach($bank_sum as $id => $values) {
  //if(count($values) == 2 && ($values[$start_date]['rub'] != 0 || $values[$start_date]['fx'] != 0 || $values[$end_date]['rub'] != 0 || $values[$end_date]['fx'] != 0)) {
  if(!in_array($id, $lost_licenses[$end_date]) && !in_array($id, $liquidated[$end_date])) {
    
    // Change
    
    $bank_sum[$id]['change']['rub'] = $values[$end_date]['rub'] - $values[$start_date]['rub'];
    $bank_sum[$id]['percent_change']['rub'] = round((($values[$end_date]['rub'] / $values[$start_date]['rub'] - 1) *100), 1);

    $bank_sum[$id]['change']['fx'] = $values[$end_date]['fx_adj'] - $values[$start_date]['fx'];
    $bank_sum[$id]['percent_change']['fx'] = round((($values[$end_date]['fx_adj'] / $values[$start_date]['fx'] - 1) *100), 1);

    $bank_sum[$id]['change']['total'] = $values[$end_date]['total_adj'] - $values[$start_date]['total'];
    $bank_sum[$id]['percent_change']['total'] = round((($values[$end_date]['total_adj'] / $values[$start_date]['total'] - 1) *100), 1);
    
    $bank_change[$id] = $bank_sum[$id]['change']['total'];
    
    // % change
    $bank_percent_change[$id] = round((($values[$end_date]['total_adj']/$values[$start_date]['total'] - 1) * 100), 1);
  }
  //} else {
    // echo "{$id}, {$banks_names[$id]}<br>";
  //}
}

// my_print_r($bank_sum);
// my_print_r($bank_change);



// Change for banking system
// -------------------------

/*$system_rub = $system_fx = $system_total = 0;

foreach($bank_change as $id => $adj_total_change) {
  $system_rub += $bank_sum[$id]['change']['rub'];
  $system_fx += $bank_sum[$id]['change']['fx'];
  $system_total += $bank_sum[$id]['change']['total'];
}

echo number_format($system_rub / 1000, 0, ',', ' ') . "<br>";
echo number_format($system_fx / 1000, 0, ',', ' ') . "<br>";
echo number_format($system_total / 1000, 0, ',', ' ') . "<br>";*/



// Leaders of outflow
// ------------------

asort($bank_change);

$ids = array_keys($bank_change);

$top_decrease = array_slice($ids, 0, 10);

$top_increase = array_slice($ids, -10);
krsort($top_increase);

// my_print_r($top_decrease);



// Starting HTML
// -------------

$names = $amounts = "";

foreach($top_decrease as $id) {
  
  $names .= "'{$banks_names[$id]}', ";
  
  $amounts .= round($bank_sum[$id]['change']['total']/1000000, 1) . ", ";
  
}


echo "<!doctype html><html><head>";
echo "<meta charset='utf-8'>";
echo "<title>Итоги декабря</title>";
echo "<link rel='stylesheet' href='../css/style-03.css'>";
echo "<script src='../jquery-1.10.2.min.js'></script>";
echo <<<EOD
<script type="text/javascript">
$(function () {
    $('#container').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Лидеры притока вкладов населения'
        },
        xAxis: {
            categories: [{$names}],
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'млрд. руб.',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            },
            min: -100,
            max: 0
        },
        tooltip: {
            valueSuffix: ' млрд. руб.'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'bottom',
            x: 40,
            y: -100,
            floating: true,
            borderWidth: 1,
            backgroundColor: ((Highcharts.theme && Highcharts.theme.legendBackgroundColor) || '#FFFFFF'),
            shadow: true
        },
        credits: {
            enabled: false
        },
        series: [{
            name: 'Отток вкладов',
            data: [{$amounts}]
        }]
    });
});
		</script>
<script src="../js/highcharts.js"></script>
<script src="../js/modules/exporting.js"></script>
EOD;
echo "</head><body>";



// Print change for each bank
// --------------------------

echo "<table class='result'>";

echo "<tr><th>В млн. рублей</th><th>Рублевые<br>вклады</th><th>%</th><th>Валютные<br>вклады</th><th>%</th><th>Итого<br>вклады</th><th>%</th></tr>";

foreach($top_increase as $id) {
  echo "<tr>";
  
  echo "<td>$banks_names[$id]</td>";

  // Ruble
  echo "<td>" . number_format($bank_sum[$id]['change']['rub'] / 1000, 0, ',', ' ') . "</td>";
  echo "<td>" . number_format($bank_sum[$id]['percent_change']['rub'], 1, ',', ' ') . "%</td>";
  
  // FX
  echo "<td>" . number_format($bank_sum[$id]['change']['fx'] / 1000, 0, ',', ' ') . "</td>";
  echo "<td>" . number_format($bank_sum[$id]['percent_change']['fx'], 1, ',', ' ') . "%</td>";
  
  // Total
  echo "<td>" . number_format($bank_sum[$id]['change']['total'] / 1000, 0, ',', ' ') . "</td>";
  echo "<td>" . number_format($bank_sum[$id]['percent_change']['total'], 1, ',', ' ') . "%</td>";
  
  echo "</tr>";
}

echo "</table>";

echo "<div id='container' style='min-width: 310px; max-width: 800px; height: 400px; margin: 0 auto'></div>";
