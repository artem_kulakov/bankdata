<?php

// Instruction
include_once $_SERVER['DOCUMENT_ROOT'] . '/../table/101/instruction.php';
$bs_instruction = $instruction;

include_once $_SERVER['DOCUMENT_ROOT'] . '/../table/102/instruction.php';
$pl_instruction = $instruction;

include_once $_SERVER['DOCUMENT_ROOT'] . '/../table/216/instruction.php';
$pl_2016_instruction = $instruction;



// Split provisions on overdue loans to corporate and retail
// ---------------------------------------------------------
$overdue_loans = array();

foreach($dates as $date) {
	// Calculate overdue corporate loans
	foreach($bs_instruction['Кредиты и авансы клиентам']['Кредиты юридическим лицам']['Просроченные кредиты'] as $account) {
		$overdue_loans[$date]['corporate'] += $data[101][$date][$account];
	}
	
	// Calculate overdue retail loans
	foreach($bs_instruction['Кредиты и авансы клиентам']['Кредиты физическим лицам']['Просроченные кредиты'] as $account) {
		$overdue_loans[$date]['retail'] += $data[101][$date][$account];
	}
	
	// Divide provisions between corporate and retail, and attach to MySQL data
	$data[101][$date]['45818/Ю'] = $data[101][$date][45818] / ($overdue_loans[$date]['corporate'] + $overdue_loans[$date]['retail']) * $overdue_loans[$date]['corporate'];
	$data[101][$date]['45818/Ф'] = $data[101][$date][45818] / ($overdue_loans[$date]['corporate'] + $overdue_loans[$date]['retail']) * $overdue_loans[$date]['retail'];
}

// my_print_r($overdue_loans);
// my_print_r($data);



// Convert branch of instruction to simple array of accounts
// ---------------------------------------------------------

// For Loans

// $loans['total'] = array();      // to place 'total' at the beginning of array

$loans['banks']['total_net']  = get_accounts($bs_instruction['Кредиты и авансы клиентам']['Межбанковские кредиты']);
$loans['banks']['overdue']    = get_accounts($bs_instruction['Кредиты и авансы клиентам']['Межбанковские кредиты']['Просроченная задолженность']);
$loans['banks']['provisions'] = get_accounts($bs_instruction['Кредиты и авансы клиентам']['Межбанковские кредиты']['Резерв под обесценение']);

$loans['corporate']['total_net']  = get_accounts($bs_instruction['Кредиты и авансы клиентам']['Кредиты юридическим лицам']);
$loans['corporate']['overdue']    = get_accounts($bs_instruction['Кредиты и авансы клиентам']['Кредиты юридическим лицам']['Просроченные кредиты']);
$loans['corporate']['provisions'] = get_accounts($bs_instruction['Кредиты и авансы клиентам']['Кредиты юридическим лицам']['Резерв под обесценение']);

$loans['retail']['total_net']     = get_accounts($bs_instruction['Кредиты и авансы клиентам']['Кредиты физическим лицам']);
$loans['retail']['overdue']       = get_accounts($bs_instruction['Кредиты и авансы клиентам']['Кредиты физическим лицам']['Просроченные кредиты']);
$loans['retail']['provisions']    = get_accounts($bs_instruction['Кредиты и авансы клиентам']['Кредиты физическим лицам']['Резерв под обесценение']);

$loans['total']['total_net']  = get_accounts($bs_instruction['Кредиты и авансы клиентам']);
$loans['total']['overdue']    = array_merge($loans['banks']['overdue'], $loans['corporate']['overdue'], $loans['retail']['overdue'], array(20317));
$loans['total']['provisions'] = array_merge($loans['banks']['provisions'], $loans['corporate']['provisions'], $loans['retail']['provisions'], array(20321));


// For NIM

$interest_bearing_assets['banks'] = get_accounts($bs_instruction['Средства в банках']);

$interest_bearing_assets['loans_net'] = get_accounts($bs_instruction['Кредиты и авансы клиентам']);
$interest_bearing_assets['loans_provisions_banks'] = get_accounts($bs_instruction['Кредиты и авансы клиентам']['Межбанковские кредиты']['Резерв под обесценение']);
$interest_bearing_assets['loans_gross'] = array_diff($interest_bearing_assets['loans_net'], $interest_bearing_assets['loans_provisions_banks'], $loans['corporate']['provisions'], $loans['retail']['provisions']);

$interest_bearing_assets['bonds_net'] = get_accounts($bs_instruction['Ценные бумаги']['Облигации']);
$interest_bearing_assets['bonds_provisions'] = get_accounts($bs_instruction['Ценные бумаги']['Облигации']['Резерв под обесценение']);
$interest_bearing_assets['bonds_gross'] = array_diff($interest_bearing_assets['bonds_net'], $interest_bearing_assets['bonds_provisions']);

$interest_bearing_assets['total'] = array_merge($interest_bearing_assets['banks'], $interest_bearing_assets['loans_gross'], $interest_bearing_assets['bonds_gross']);


$interest_income = get_accounts($pl_instruction['Процентные доходы']);
$interest_expense = get_accounts($pl_instruction['Процентные расходы']);
$net_interest_income = array_merge($interest_income, $interest_expense);

//$interest_income_banks = get_accounts($pl_instruction['Процентные доходы']['От размещения средств в кредитных организациях']);
//$interest_expense_banks = get_accounts($pl_instruction['Процентные расходы']['По привлеченным средствам банков']);
//$net_interest_income_banks = array_merge($interest_income_banks, $interest_expense_banks);

//$interest_income_loans = get_accounts($pl_instruction['Процентные доходы']['От ссуд, предоставленных клиентам, не являющимся кредитными организациями']);
//$interest_expense_loans = get_accounts($pl_instruction['Процентные расходы']['По привлеченным средствам клиентов']);
//$net_interest_income_banks = array_merge($interest_income_loans, $interest_expense_loans);

//$interest_income_bonds = get_accounts($pl_instruction['Процентные доходы']['От вложений в ценные бумаги']);
//$interest_expense_bonds = get_accounts($pl_instruction['Процентные расходы']['По выпущенным долговым обязательствам']);
//$net_interest_income_bonds = array_merge($interest_income_bonds, $interest_expense_bonds);


$interest_income_2016 = get_accounts($pl_2016_instruction['Процентные доходы']);
$interest_expense_2016 = get_accounts($pl_2016_instruction['Процентные расходы']);
$net_interest_income_2016 = array_merge($interest_income_2016, $interest_expense_2016);

//$interest_income_2016_banks = get_accounts($pl_2016_instruction['Процентные доходы']['От размещения средств в банках']);
//$interest_expense_2016_banks = get_accounts($pl_2016_instruction['Процентные расходы']['По средствам банков']);
//$net_interest_income_2016_banks = array_merge($interest_income_2016_banks, $interest_expense_2016_banks);

//$interest_income_2016_loans = get_accounts($pl_2016_instruction['Процентные доходы']['От ссуд, предоставленных клиентам']);
//$interest_expense_2016_loans = get_accounts($pl_2016_instruction['Процентные расходы']['По средствам клиентов']);
//$net_interest_income_2016_loans = array_merge($interest_income_2016_loans, $interest_expense_2016_loans);

//$interest_income_2016_bonds = get_accounts($pl_2016_instruction['Процентные доходы']['От вложений в ценные бумаги']);
//$interest_expense_2016_bonds = get_accounts($pl_2016_instruction['Процентные расходы']['По выпущенным долговым обязательствам']);
//$net_interest_income_2016_bonds = array_merge($interest_income_2016_bonds, $interest_expense_2016_bonds);


// For securities

$securities['bonds_gross'] = $interest_bearing_assets['bonds_gross'];

$securities['shares_net'] = get_accounts($bs_instruction['Ценные бумаги']['Акции']);
$securities['shares_provisions'] = get_accounts($bs_instruction['Ценные бумаги']['Акции']['Резерв под обесценение']);
$securities['shares_gross'] = array_diff($securities['shares_net'], $securities['shares_provisions']);

$securities['derivatives_gross'] = get_accounts($bs_instruction['Ценные бумаги']['Деривативы']);

$securities['total_gross'] = array_merge($securities['bonds_gross'], $securities['shares_gross'], $securities['derivatives_gross']);



function get_accounts($array, $accounts) {
  
  foreach($array as $item) {
    if(is_array($item)) {
      $accounts = get_accounts($item, $accounts);
    } else {
      $accounts[] = $item;
    }
  }
  
  return $accounts;
}

// my_print_r($corporate_loans);
// my_print_r($interest_bearing_assets['loans_gross']);
// my_print_r($net_interest_income);



// Calculation for NIM
// ------------------

foreach($dates_to_download as $date) {
  
  $interest_bearing_assets[$date] = 0;
  
  foreach($interest_bearing_assets['total'] as $account) {
    $interest_bearing_assets[$date] += $data[101][$date][$account];
  }
  
}

// my_print_r($interest_bearing_assets);
// my_print_r($total);


foreach($dates as $date) {
  
  $net_interest_income[$date] = 0;
  
  if(strtotime($date) <= strtotime('2016-01-01')) {
    $nii = $net_interest_income;
  } else {
    $nii = $net_interest_income_2016;
  }
  
  foreach($nii as $account) {
    $net_interest_income[$date] += $data[102][$date][$account];
  }
  
  // Beginning of the period
  $year  = substr($date, 0, 4);
  $month = substr($date, 5, 2);
  
  if($month == "01") {
    $year -= 1;
    $months = 12;
  } else {
    $months = $month - 1;
  }
  
  $beginning_of_the_period = "{$year}-01-01";
  
  $nim[$date] = round(($net_interest_income[$date] / $months * 12) / (($interest_bearing_assets[$beginning_of_the_period] + $interest_bearing_assets[$date]) / 2) * 100, 1);
  $average_interest_bearing_assets[$date] = ($interest_bearing_assets[$beginning_of_the_period] + $interest_bearing_assets[$date]) / 2;
}

// my_print_r($net_interest_income);
// my_print_r($nim);
// my_print_r($average_interest_bearing_assets);



// Calculation for Loans
// ---------------------

foreach($dates as $date) {
  
  // For Loans
  foreach($loans as $type => $parts) {
    foreach($parts as $part => $accounts) {
    
      $total[$type][$date][$part] = 0;
  
      foreach($accounts as $account) {
        $total[$type][$date][$part] += $data[101][$date][$account];
      }    
    }
  
  // Calculating loans ratios
  // ------------------------
  $total[$type][$date]['total_gross'] = $total[$type][$date]['total_net'] - $total[$type][$date]['provisions'];
  $total[$type][$date]['overdue_share'] = round($total[$type][$date]['overdue'] / $total[$type][$date]['total_gross'] * 100, 1);
  $total[$type][$date]['provisions_share'] = -round($total[$type][$date]['provisions'] / $total[$type][$date]['total_gross'] * 100, 1);
  
  }
}

// my_print_r($total);



// Calculation for Securities
// --------------------------

foreach($dates as $date) {
  
  foreach($securities as $type => $accounts) {
    
    if(substr($type, -5) == "gross") {
    
      $securities_values[$type][$date] = 0;
    
      foreach($accounts as $account) {
        $securities_values[$type][$date] += $data[101][$date][$account];
      }
    }
  }
  
  $securities_output['bonds_share'][$date] = round($securities_values['bonds_gross'][$date] / $securities_values['total_gross'][$date] * 100, 1);
  $securities_output['shares_share'][$date] = round($securities_values['shares_gross'][$date] / $securities_values['total_gross'][$date] * 100, 1);
  $securities_output['derivatives_share'][$date] = round($securities_values['derivatives_gross'][$date] / $securities_values['total_gross'][$date] * 100, 1);
  
}

// my_print_r($securities_output);



// Array for output
// ----------------

// Loans

$names = array(
  'banks' => array(
    'total_gross'       => 'Межбанковские кредиты',
    'overdue_share'     => 'Просроченные',
    'provisions_share'  => 'Резервы'
  ),
  'corporate' => array(
    'total_gross'       => 'Корпоративные кредиты',
    'overdue_share'     => 'Просроченные',
    'provisions_share'  => 'Резервы'
  ),
  'retail' => array(
    'total_gross'       => 'Розничные кредиты',
    'overdue_share'     => 'Просроченные',
    'provisions_share'  => 'Резервы'
  ),  
  'total' => array(
    'total_gross'       => 'Итого кредиты',
    'overdue_share'     => 'Просроченные',
    'provisions_share'  => 'Резервы'
  )
);


$index = 0;

foreach($total as $type => $values) {
  
  $output[$index]['name'] = "";
  
  ++$index;
  
  foreach(array('total_gross', 'overdue_share', 'provisions_share') as $item) {
    
    $output[$index]['name'] = $names[$type][$item];
    
    foreach($dates as $date) {
      $output[$index][$date] = $values[$date][$item];
    }
    
    ++$index;
    
  }
}


// Securities

$securities_names = array(
  'bonds_share' => 'Облигации',
  'shares_share' => 'Акции',
  'derivatives_share' => 'Деривативы'
);

$output[$index]['name'] = "";

foreach($securities_output as $type => $values) {
    
  ++$index;
  
  $output[$index]['name'] = $securities_names[$type];
  
  foreach($dates as $date) {
    $output[$index][$date] = $values[$date];
  }
}

// my_print_r($output);



// Result table
// ------------

echo "<div id='ratios'>";

echo "<table class='result'>";

// Header
echo "<tr><th>";

// Scale

echo "<select class='scale' onchange='if (this.value) window.location.href=this.value'>";

foreach($scale as $key => $value){
  if($key == $multiple) {
    $selected = " selected";
  } else {
    $selected = "";
  }

  echo "<option value='index.php?m={$mode}&q={$bank_id}{$http_dates_query}{$turnover_query}&s={$key}'{$selected}>{$value}</option>";
}

echo "</select>";

echo "</th>";

$months = array(1 => 'января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря');

foreach($dates as $date) {
  $year  = substr($date, 0, 4);
  $month = intval(substr($date, 5, 2));
  
	echo "<th>1 {$months[$month]} {$year}</th>";
}

// Changes columns if user chose more than one date
if(count($dates) > 1) {
  echo "<th>Изменение</th>";
}

echo "</tr>";


// Last and second last dates
$last_date = end($dates);
$second_last_date = prev($dates);


// Loans

foreach($output as $values) {
  
  $percentage = preg_match("/Просроченные|Резервы|Облигации|Акции|Деривативы/", $values['name']);
  
  echo "<tr>";
  
  echo "<td>{$values['name']}</td>";

  foreach($dates as $date) {
    if($values['name'] == "") {
      echo "<td>&nbsp;</td>";
    } elseif($percentage) { //$values['name'] == 'Просроченные' || $values['name'] == 'Резервы') {
      echo "<td>" . number_format($values[$date], 1, ",", "") . "%</td>";
    } else {
      echo "<td>" . number_format($values[$date] / $multiple, 0, ",", " ") . "</td>";
    }
  }

  // Change
  if(count($dates) > 1) {
    if($percentage) { //$values['name'] == 'Просроченные' || $values['name'] == 'Резервы') {
      $change = $values[$last_date] - $values[$second_last_date];
    } else {
      $change = ($values[$last_date] / $values[$second_last_date] - 1) * 100;
    }
  
    // Positive sign for positive change
    if($change > 0) {
      $plus = "+";
    } else {
      $plus = "";
    }
  
    if($values['name'] == "") {
      echo "<td></td>";
    } else {
      echo "<td>" . $plus . number_format($change, 1, ",", "") . "%</td>";
    }

  }

  echo "</tr>";
}



// Gap between tables
// ------------------

echo "<tr class='gap'>";

echo "<th></th>";

foreach($dates as $date) {
  echo "<th></th>";
}

if(count($dates) > 1) {
  echo "<th></th>";
}

echo "</tr>";



// Second header
echo "<tr>";

echo "<th></th>";

$period = array(
  '3' => "I квартал",
  '6' => "I полугодие",
  '9' => "9 месяцев",
  '12' => "",  
);

$quarters = array(1, 4, 7, 10);

foreach($dates as $date) {
  $year  = substr($date, 0, 4);
  $month = substr($date, 5, 2);
  
  if($month == "01") {
    $year -= 1;
    $months = 12;
  } else {
    $months = $month - 1;
  }
  
  if(in_array($month, $quarters)) {
    echo "<th>{$period[$months]} {$year}</th>";
  } else {
    echo "<th></th>";
  }
}

if(count($dates) > 1) {
  echo "<th>Изменение</th>";
}

echo "</tr>";


// NIM
echo "<tr>";

echo "<td>Чистая процентная маржа</td>";

foreach($dates as $date) {
  
  $month = substr($date, 5, 2);
  
  if(in_array($month, $quarters)) {
    echo "<td>" . number_format($nim[$date], 1, ",", "") . "%</td>";
  } else {
    echo "<td></td>";
  }
}

// Change
if(count($dates) > 1) {
  $change = $nim[$last_date] - $nim[$second_last_date];

  // Positive sign for positive change
  if($change > 0) {
    $plus = "+";
  } else {
    $plus = "";
  }

  echo "<td>" . $plus . number_format($change, 1, ",", "") . "%</td>";
}

echo "</tr>";


// Average interest bearing assets
echo "<tr>";

echo "<td>Средние процентные активы</td>";

foreach($dates as $date) {
  
  $month = substr($date, 5, 2);
  
  if(in_array($month, $quarters)) {
    echo "<td>" . number_format($average_interest_bearing_assets[$date] / $multiple, 0, ",", " ") . "</td>";
  } else {
    echo "<td></td>";
  }
}

// Change
if(count($dates) > 1) {
  $change = ($average_interest_bearing_assets[$last_date] / $average_interest_bearing_assets[$second_last_date] - 1) * 100;

  // Positive sign for positive change
  if($change > 0) {
    $plus = "+";
  } else {
    $plus = "";
  }

  echo "<td>" . $plus . number_format($change, 1, ",", " ") . "</td>";
}


echo "</tr>";

echo "</table>";

echo "</div>";
